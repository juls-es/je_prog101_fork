# chessgirls
Chess project for PROG101. 

It's a game based on the following requirements:
- User interaction through console
- Black pieces are in the top amd White pieces are in the bottom
- User can choose to play with the white pieces or the black pieces
- User menu contains:
  1. Move a piece
  2. Get possibles moves
  3. Restart the Game
  4. Continue a Game saved
  5. Save a Game
  6. Exit
