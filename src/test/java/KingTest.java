import org.junit.Test;

import static org.junit.Assert.*;

public class KingTest {
    @Test
    public void testBlackingWhenIsNotMovedHasFivePossibilities() {
        Piece[][] board = new Piece[8][8];
        King king = new King(Color.B, new Position(0, 4), board);
        board[0][4] = king;
        assertEquals(5, king.getPossibleMoves().size());
    }

    @Test
    public void testBlackingWhenRowZeroHasFivePossibilities() {
        Piece[][] board = new Piece[8][8];
        King king = new King(Color.B, new Position(0, 6), board);
        king.setDoesMoved(true);
        board[0][6] = king;
        assertEquals(5, king.getPossibleMoves().size());
    }
    @Test
    public void testBlackingWhenNoIsInitialRowZeroHasEightPossibilitiesIfNotOccupied() {
        Piece[][] board = new Piece[8][8];
        King king = new King(Color.B, new Position(1, 1), board);
        king.setDoesMoved(true);
        board[1][1] = king;
        assertEquals(8, king.getPossibleMoves().size());
    }

    @Test
    public void testBlackingIfIsInCornerTopLeftHasThreePossibilities() {
        Piece[][] board = new Piece[8][8];
        King king = new King(Color.B, new Position(0, 0), board);
        king.setDoesMoved(true);
        board[0][0] = king;
        assertEquals(3, king.getPossibleMoves().size());
    }
    @Test
    public void testBlackingIfIsInCornerTopRightHasThreePossibilities() {
        Piece[][] board = new Piece[8][8];
        King king = new King(Color.B, new Position(0, 7), board);
        king.setDoesMoved(true);
        board[0][7] = king;
        assertEquals(3, king.getPossibleMoves().size());
    }
    @Test
    public void testBlackingIfIsInCornerBottomLeftHasThreePossibilities() {
        Piece[][] board = new Piece[8][8];
        King king = new King(Color.B, new Position(7, 0), board);
        king.setDoesMoved(true);
        board[7][0] = king;
        assertEquals(3, king.getPossibleMoves().size());
    }
    @Test
    public void testBlackingIfIsInCornerBottomRighttHasThreePossibilities() {
        Piece[][] board = new Piece[8][8];
        King king = new King(Color.B, new Position(7, 7), board);
        king.setDoesMoved(true);
        board[7][7] = king;
        assertEquals(3, king.getPossibleMoves().size());
    }

    @Test
    public void testWhiteKingWhenIsNotMovedHasFivePossibilities() {
        Piece[][] board = new Piece[8][8];
        King king = new King(Color.W, new Position(7, 4), board);
        board[7][4] = king;
        assertEquals(5, king.getPossibleMoves().size());
    }

    @Test
    public void testWhiteKingWhenRowSevenHasFivePossibilities() {
        Piece[][] board = new Piece[8][8];
        King king = new King(Color.B, new Position(7, 3), board);
        king.setDoesMoved(true);
        board[7][3] = king;
        king.getPossibleMoves();
        assertEquals(5, king.getPossibleMoves().size());
    }
    @Test
    public void testWhiteKingWhenNoIsInitialRowSevenHasEightPossibilitiesIfNotOccupied() {
        Piece[][] board = new Piece[8][8];
        King king = new King(Color.W, new Position(5, 5), board);
        king.setDoesMoved(true);
        board[5][5] = king;
        assertEquals(8, king.getPossibleMoves().size());
    }

    @Test
    public void testWhiteKingIfIsInCornerTopLeftHasThreePossibilities() {
        Piece[][] board = new Piece[8][8];
        King king = new King(Color.W, new Position(0, 0), board);
        king.setDoesMoved(true);
        board[0][0] = king;
        assertEquals(3, king.getPossibleMoves().size());
    }
    @Test
    public void testWhiteKingIfIsInCornerTopRightHasThreePossibilities() {
        Piece[][] board = new Piece[8][8];
        King king = new King(Color.W, new Position(0, 7), board);
        king.setDoesMoved(true);
        board[0][7] = king;
        assertEquals(3, king.getPossibleMoves().size());
    }
    @Test
    public void testWhiteKingIfIsInCornerBottomLeftHasThreePossibilities() {
        Piece[][] board = new Piece[8][8];
        King king = new King(Color.W, new Position(7, 0), board);
        king.setDoesMoved(true);
        board[7][0] = king;
        assertEquals(3, king.getPossibleMoves().size());
    }

    @Test
    public void testWhiteKingIfIsInCornerBottomRightHasThreePossibilities() {
        Piece[][] board = new Piece[8][8];
        King king = new King(Color.W, new Position(7, 7), board);
        king.setDoesMoved(true);
        board[7][7] = king;
        assertEquals(3, king.getPossibleMoves().size());
    }


    @Test
    public void testPossibleBlackKingMovesSevenIfPieceInDiagonalLeftDifferentColor() {
        Piece[][] board = new Piece[8][8];
        King king = new King(Color.B, new Position(1,5), board);
        king.setDoesMoved(true);
        Pawn pawn2 = new Pawn(Color.W, new Position(2,4), board);
        board[1][5] = king;
        board[2][4] =pawn2;
        assertEquals(8, king.getPossibleMoves().size());
    }
    @Test
    public void testPossibleWhiteKingMovesInDiagonalRightAndLeftDifferentColor() {
        Piece[][] board = new Piece[8][8];
        King king = new King(Color.W, new Position(3,3), board);
        king.setDoesMoved(true);
        Pawn pawn = new Pawn(Color.W, new Position(3,2), board);
        Pawn pawn2 = new Pawn(Color.B, new Position(3,4), board);
        Pawn pawn3 = new Pawn(Color.W, new Position(4,4), board);
        board[3][3] = king;
        board[3][2] = pawn;
        board[3][4] =pawn2;
        board[4][4] =pawn3;
        assertEquals(6, king.getPossibleMoves().size());
    }
    @Test
    public void testPossibleBlackKingMovesInDiagonalRightAndLeftDifferentColor() {
        Piece[][] board = new Piece[8][8];
        King king = new King(Color.B, new Position(3,3), board);
        king.setDoesMoved(true);
        Pawn pawn = new Pawn(Color.W, new Position(3,2), board);
        Pawn pawn2 = new Pawn(Color.B, new Position(3,4), board);
        Pawn pawn3 = new Pawn(Color.W, new Position(4,4), board);
        board[3][3] = king;
        board[3][2] = pawn;
        board[3][4] =pawn2;
        board[4][4] =pawn3;
        assertEquals(7, king.getPossibleMoves().size());
    }

    @Test
    public void testTheKingTakesAStepForward() {
        Piece[][] board = new Piece[8][8];
        King king = new King(Color.W, new Position(3,4), board);
        board[3][4] = king;
        king.move(new Position(2, 4));
        assertEquals(2, king.getPosition().getRow());
        assertEquals(4, king.getPosition().getColumn());
    }

    @Test
    public void testDoesNotMoveIfThePositionIsOccupiedSameColor() {
        Piece[][] board = new Piece[8][8];
        King king = new King(Color.W, new Position(3,4), board);
        Pawn pawn3 = new Pawn(Color.W, new Position(2,4), board);
        board[3][4] = king;
        board[2][4] = pawn3;
        king.move(new Position(2, 4));
        assertEquals(3, king.getPosition().getRow());
        assertEquals(4, king.getPosition().getColumn());
    }

    @Test
    public void testMovesIfThePositionIsOccupiedDifferentColor() {
        Piece[][] board = new Piece[8][8];
        King king = new King(Color.W, new Position(3,4), board);
        king.setDoesMoved(true);
        Pawn pawn3 = new Pawn(Color.B, new Position(2,4), board);
        board[3][4] = king;
        board[2][4] = pawn3;
        king.move(new Position(2, 4));
        assertEquals(2, king.getPosition().getRow());
        assertEquals(4, king.getPosition().getColumn());
    }

    @Test
    public void testNotMovesIfThePositionIsOccupiedDifferentColorBlack() {
        Piece[][] board = new Piece[8][8];
        King king = new King(Color.B, new Position(3,4), board);
        king.setDoesMoved(true);
        Pawn pawn3 = new Pawn(Color.W, new Position(4,4), board);
        board[3][5] = king;
        board[4][4] = pawn3;
        king.move(new Position(4, 4));
        assertEquals(4, king.getPosition().getRow());
        assertEquals(4, king.getPosition().getColumn());
    }

    @Test
    public void testMovingAPieceItChangesStateDoesMovesToTrue() {
        Piece[][] board = new Piece[8][8];
        King king = new King(Color.W, new Position(7,4), board);
        board[7][4] = king;
        king.move(new Position(6, 4));
        assertTrue(king.getDoesMoved());
    }
    @Test
    public void testDestroyingAPieceItChangesStateDoesExistToFalse() {
        Piece[][] board = new Piece[8][8];
        King king = new King(Color.B, new Position(3,4), board);
        board[3][5] = king;
        king.destroy(king);
        assertFalse(king.doesExist());

    }
    @Test
    public void testDestroyingAPieceItLosePosition() {
        Piece[][] board = new Piece[8][8];
        King king = new King(Color.B, new Position(3,4), board);
        board[3][5] = king;
        king.destroy(king);
        assertEquals(null, king.getPosition());
    }
}
