import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import java.util.List;

public class HorseTest {

    private List<Position> possibles = null;
    private Piece[][] board = new Piece[8][8];
    Position position;
    Horse horseInitial = new Horse(Color.B, new Position(0,1), board);
    Horse horseCenter = new Horse(Color.B, new Position(3,4), board);
    Pawn blackPawn = new Pawn(Color.B, new Position(1,3), board);
    Pawn whitePawn = new Pawn(Color.B, new Position(2,2), board);

    @Test
    public void testCantPossibleMovesWhenBlackHorseIsInitialPosition() {
        int expected = 3;
        board[0][1] = horseInitial;
        int actual = horseInitial.getPossibleMoves().size();
        assertEquals(expected, actual);
    }
    @Test
    public void testFirstPossibleMoveWhenBlackHorseIsInitialPositionRowTwo() {
        int expected = 2;
        board[0][1] = horseInitial;
        possibles = horseInitial.getPossibleMoves();
        int actual = possibles.get(0).getRow();
        assertEquals(expected, actual);
    }
    @Test
    public void testFirstPossibleMoveWhenBlackHorseIsInitialPositionColummnZero() {
        int expected = 0;
        possibles = horseInitial.getPossibleMoves();
        int actual = possibles.get(0).getColumn();
        assertEquals(expected, actual);
    }
    @Test
    public void testSecondPossibleMoveWhenBlackHorseIsInitialPositionRowOne() {
        int expected = 1;
        possibles = horseInitial.getPossibleMoves();
        int actual = possibles.get(1).getRow();
        assertEquals(expected, actual);
    }
    @Test
    public void testSecondPossibleMoveWhenBlackHorseIsInitialPositionColumnThree() {
        int expected = 3;
        possibles = horseInitial.getPossibleMoves();
        int actual = possibles.get(1).getColumn();
        assertEquals(expected, actual);
    }
    @Test
    public void testThirdPossibleMoveWhenBlackHorseIsInitialPositionRowTwo() {
        int expected = 2;
        possibles = horseInitial.getPossibleMoves();
        int actual = possibles.get(2).getRow();
        assertEquals(expected, actual);
    }
    @Test
    public void testThirdPossibleMoveWhenBlackHorseIsInitialPositionColumnTwo() {
        int expected = 2;
        possibles = horseInitial.getPossibleMoves();
        int actual = possibles.get(2).getColumn();
        assertEquals(expected, actual);
    }
    @Test
    public void testCantPossibleMovesWhenBlackHorseIsCenterChessBoard() {
        int expected = 8;
        int actual = horseCenter.getPossibleMoves().size();
        assertEquals(expected, actual);
    }
    @Test
    public void testOnePossibleMoveWhenBlackHorseIsInCenterChessBoardIsRowTwo() {
        int expected = 1;
        possibles = horseCenter.getPossibleMoves();
        int actual = possibles.get(0).getRow();
        assertEquals(expected, actual);
    }
    @Test
    public void testOnePossibleMoveWhenBlackHorseIsInCenterChessBoardIsColummnThree() {
        int expected = 3;
        possibles = horseCenter.getPossibleMoves();
        int actual = possibles.get(0).getColumn();
        assertEquals(expected, actual);
    }
    @Test
    public void testBlackHorseCanNotMoveFromInitialPositionToRowOneColumnThree() {
        int expected = 2;
        board[0][1] = horseInitial;
        board[1][3] = blackPawn;
        horseInitial.setBoard(board);
        int actual = horseInitial.getPossibleMoves().size();
        assertEquals(expected, actual);
    }
    @Test
    public void testBlackHorseCanMoveFromInitialPositionToRowTwoColumnTwo() {
        int expected = 2;
        possibles = horseCenter.getPossibleMoves();
        int actual = possibles.get(1).getRow();
        assertEquals(expected, actual);
    }
    @Test
    public void testRowTwoColumnZeroIsPossiblePositionToMoveBlackHorseWhenIsInitialPosition() {
        board[0][1] = horseInitial;
        position = new Position(2, 0);
        boolean isPossible = horseInitial.isPossiblePosition(position);
        assertTrue(isPossible);
    }
    @Test
    public void testOneTwoColumnThreeIsPossiblePositionToMoveBlackHorseWhenIsInitialPosition() {
        board[0][1] = horseInitial;
        position = new Position(1, 3);
        boolean isPossible = horseInitial.isPossiblePosition(position);
        assertTrue(isPossible);
    }
    @Test
    public void testRowTwoColumnTwoIsPossiblePositionToMoveBlackHorseWhenIsInitialPosition() {
        board[0][1] = horseInitial;
        position = new Position(2, 2);
        boolean isPossible = horseInitial.isPossiblePosition(position);
        assertTrue(isPossible);
    }
    @Test
    public void testRowOneColumnThreeIsPossiblePositionToMoveBlackHorseWhenIsCenterPosition() {
        board[3][4] = horseCenter;
        position = new Position(1, 3);
        boolean isPossible = horseInitial.isPossiblePosition(position);
        assertTrue(isPossible);
    }
    @Test
    public void testIsPossibleMovetoRowTwoColumnZeroWhenHorseIsInitialPosition() {
        board[0][1] = horseInitial;
        position = new Position(2, 0);
        boolean isPossible = horseInitial.isPossibleMove(position);
        assertTrue(isPossible);
    }
    @Test
    public void testRowWhenBlackHorseNovesFromInitialPositionToRowOneColumnThree() {
        int actual = 2;
        board[0][1] = horseInitial;
        position = new Position(2, 0);
        horseInitial.move(position);
        int expected = horseInitial.getPosition().getRow();
        assertEquals(expected, actual);
    }
    @Test
    public void testColumnWhenBlackHorseNovesFromInitialPositionToRowOneColumnThree() {
        int actual = 0;
        board[0][1] = horseInitial;
        position = new Position(2, 0);
        horseInitial.move(position);
        int expected = horseInitial.getPosition().getColumn();
        assertEquals(expected, actual);
    }
}
