import org.junit.Test;

import static org.junit.Assert.*;

import java.util.List;
public class ParserTest {
    Parser positionToConvert;
    List<int[]> resultOfConvertion;
    String inputString = "S(2a)T(3a)";
    @Test
    public void testIndexRowOfSourceOfPositionInStringFormat() {
        int expected = 6;
        positionToConvert = new Parser(inputString);
        try {
            resultOfConvertion = positionToConvert.convertPosition();
            int actual = resultOfConvertion.get(0)[0];
            assertEquals(expected, actual);
        } catch (Exception e) {
        }
        
    }
    @Test
    public void testIndexColumnOfSourceOfPositionInStringFormat() {
        int expected = 0;
        positionToConvert = new Parser(inputString);
        try {
            resultOfConvertion = positionToConvert.convertPosition();
            int actual = resultOfConvertion.get(0)[1];
            assertEquals(expected, actual);
        } catch (Exception e) {
        }
        
    }
    @Test
    public void testIndexRowOfTargetOfPositionInStringFormat() {
        int expected = 5;
        positionToConvert = new Parser(inputString);
        try {
            resultOfConvertion = positionToConvert.convertPosition();
            int actual = resultOfConvertion.get(1)[0];
            assertEquals(expected, actual);
        } catch (Exception e) {
        }
        
    }
    @Test
    public void testIndexColumnOfTargetOfPositionInStringFormat() {
        int expected = 0;
        positionToConvert = new Parser(inputString);
        try {
            resultOfConvertion = positionToConvert.convertPosition();
            int actual = resultOfConvertion.get(1)[1];
            assertEquals(expected, actual);
        } catch (Exception e) {
        }
        
    }
    @Test
    public void testIndexRowOfPositionInStringFormat() {
        int expected = 6;
        String inputString = "P(2a)";
        positionToConvert = new Parser(inputString);
        try {
            int actual = positionToConvert.convertPiecePosition()[0];
            assertEquals(expected, actual);
        } catch (Exception e) {
        }
        
    }
    @Test
    public void testIndexColumnOfPositionInStringFormat() {
        int expected = 0;
        try {
            int actual = positionToConvert.convertPiecePosition()[1];
            assertEquals(expected, actual);
        } catch (Exception e) {
        }
        
    }
    @Test
    public void testRowStringResultOfConvertionOfPosition() {
        String expected = "2";
        Position position = new Position(6, 0);
        Parser Parser = new Parser(position);
        String actual = Parser.convertToDisplay()[0];
        assertEquals(expected, actual);
    }
    @Test
    public void testColumnStringResultOfConvertionOfPosition() {
        String expected = "a";
        Position position = new Position(6, 0);
        Parser Parser = new Parser(position);
        String actual = Parser.convertToDisplay()[1];
        assertEquals(expected, actual);
    }
    @Test
    public void testGetEquivalentRowInChessBoardOfRowZero() {
        int expected = 8;
        int row = 0;
        positionToConvert = new Parser(inputString);
        int actual = positionToConvert.getEquivalentRow(row);
        assertEquals(expected, actual);
    }
    @Test
    public void testGetEquivalentRowInChessBoardOfRowOne() {
        int expected = 7;
        int row = 1;
        positionToConvert = new Parser(inputString);
        int actual = positionToConvert.getEquivalentRow(row);
        assertEquals(expected, actual);
    }
    @Test
    public void testGetEquivalentRowInChessBoardOfRowTwo() {
        int expected = 6;
        int row = 2;
        positionToConvert = new Parser(inputString);
        int actual = positionToConvert.getEquivalentRow(row);
        assertEquals(expected, actual);
    }
    @Test
    public void testGetEquivalentRowInChessBoardOfRowThree() {
        int expected = 5;
        int row = 3;
        positionToConvert = new Parser(inputString);
        int actual = positionToConvert.getEquivalentRow(row);
        assertEquals(expected, actual);
    }
    @Test
    public void testGetEquivalentRowInChessBoardOfRowFour() {
        int expected = 4;
        int row = 4;
        positionToConvert = new Parser(inputString);
        int actual = positionToConvert.getEquivalentRow(row);
        assertEquals(expected, actual);
    }
    @Test
    public void testGetEquivalentRowInChessBoardOfRowFive() {
        int expected = 3;
        int row = 5;
        positionToConvert = new Parser(inputString);
        int actual = positionToConvert.getEquivalentRow(row);
        assertEquals(expected, actual);
    }
    @Test
    public void testGetEquivalentRowInChessBoardOfRowSix() {
        int expected = 2;
        int row = 6;
        positionToConvert = new Parser(inputString);
        int actual = positionToConvert.getEquivalentRow(row);
        assertEquals(expected, actual);
    }
    @Test
    public void testGetEquivalentRowInChessBoardOfRowSeven() {
        int expected = 1;
        int row = 7;
        positionToConvert = new Parser(inputString);
        int actual = positionToConvert.getEquivalentRow(row);
        assertEquals(expected, actual);
    }
    @Test
    public void testGetEquivalentOfIndexStringOne() {
        int expected = 7;
        String index = "1";
        positionToConvert = new Parser(inputString);
        int actual = positionToConvert.getEquivalent(index);
        assertEquals(expected, actual);
    }
    @Test
    public void testGetEquivalentOfIndexStringTwo() {
        int expected = 6;
        String index = "2";
        positionToConvert = new Parser(inputString);
        int actual = positionToConvert.getEquivalent(index);
        assertEquals(expected, actual);
    }
    @Test
    public void testGetEquivalentOfIndexStringThree() {
        int expected = 5;
        String index = "3";
        positionToConvert = new Parser(inputString);
        int actual = positionToConvert.getEquivalent(index);
        assertEquals(expected, actual);
    }
    @Test
    public void testGetEquivalentOfIndexStringFour() {
        int expected = 4;
        String index = "4";
        positionToConvert = new Parser(inputString);
        int actual = positionToConvert.getEquivalent(index);
        assertEquals(expected, actual);
    }
    @Test
    public void testGetEquivalentOfIndexStringFive() {
        int expected = 3;
        String index = "5";
        positionToConvert = new Parser(inputString);
        int actual = positionToConvert.getEquivalent(index);
        assertEquals(expected, actual);
    }
    @Test
    public void testGetEquivalentOfIndexStringSix() {
        int expected = 2;
        String index = "6";
        positionToConvert = new Parser(inputString);
        int actual = positionToConvert.getEquivalent(index);
        assertEquals(expected, actual);
    }
    @Test
    public void testGetEquivalentOfIndexStringSeven() {
        int expected = 1;
        String index = "7";
        positionToConvert = new Parser(inputString);
        int actual = positionToConvert.getEquivalent(index);
        assertEquals(expected, actual);
    }
    @Test
    public void testGetEquivalentOfIndexStringEight() {
        int expected = 0;
        String index = "8";
        positionToConvert = new Parser(inputString);
        int actual = positionToConvert.getEquivalent(index);
        assertEquals(expected, actual);
    }
    @Test
    public void testThatCharacterAIsConvertedInNumberZero() {
        assertEquals(0, Parser.getNumber('a'));
    }
    @Test
    public void testThatCharacterBIsConvertedInNumberOne() {
        assertEquals(1, Parser.getNumber('b'));
    }
    @Test
    public void testThatCharacterCIsConvertedInNumberTwo() {
        assertEquals(2, Parser.getNumber('c'));
    }
    @Test
    public void testThatCharacterDIsConvertedInNumberThree() {
        assertEquals(3, Parser.getNumber('d'));
    }
    @Test
    public void testThatCharacterEIsConvertedInNumberFour() {
        assertEquals(4, Parser.getNumber('e'));
    }
    @Test
    public void testThatCharacterFIsConvertedInNumberFive() {
        assertEquals(5, Parser.getNumber('f'));
    }
    @Test
    public void testThatCharacterGIsConvertedInNumberSix() {
        assertEquals(6, Parser.getNumber('g'));
    }
    @Test
    public void testThatCharacterHisConvertedInNumberSeven() {
        assertEquals(7, Parser.getNumber('h'));
    }
    @Test
    public void testThatDifferentCharacterIsConvertedInNegativeNumber() {
        assertEquals(-1, Parser.getNumber('z'));
    }
    @Test
    public void testGetLetterCorrespondingToNumberZero() {
        String expected = "a";
        int number = 0;
        positionToConvert = new Parser(inputString);
        String actual = positionToConvert.getLetter(number);
        assertEquals(expected, actual);
    }
    @Test
    public void testGetLetterCorrespondingToNumberOne() {
        String expected = "b";
        int number = 1;
        positionToConvert = new Parser(inputString);
        String actual = positionToConvert.getLetter(number);
        assertEquals(expected, actual);
    }
    @Test
    public void testGetLetterCorrespondingToNumberTwo() {
        String expected = "c";
        int number = 2;
        positionToConvert = new Parser(inputString);
        String actual = positionToConvert.getLetter(number);
        assertEquals(expected, actual);
    }
    @Test
    public void testGetLetterCorrespondingToNumberThree() {
        String expected = "d";
        int number = 3;
        positionToConvert = new Parser(inputString);
        String actual = positionToConvert.getLetter(number);
        assertEquals(expected, actual);
    }
    @Test
    public void testGetLetterCorrespondingToNumberFour() {
        String expected = "e";
        int number = 4;
        positionToConvert = new Parser(inputString);
        String actual = positionToConvert.getLetter(number);
        assertEquals(expected, actual);
    }
    @Test
    public void testGetLetterCorrespondingToNumberFive() {
        String expected = "f";
        int number = 5;
        positionToConvert = new Parser(inputString);
        String actual = positionToConvert.getLetter(number);
        assertEquals(expected, actual);
    }
    @Test
    public void testGetLetterCorrespondingToNumberSix() {
        String expected = "g";
        int number = 6;
        positionToConvert = new Parser(inputString);
        String actual = positionToConvert.getLetter(number);
        assertEquals(expected, actual);
    }
    @Test
    public void testGetLetterCorrespondingToNumberSeven() {
        String expected = "h";
        int number = 7;
        positionToConvert = new Parser(inputString);
        String actual = positionToConvert.getLetter(number);
        assertEquals(expected, actual);
    }
}
