import java.util.List;

import org.junit.Test;

import static org.junit.Assert.*;

public class QueenTest {
    
    @Test
    public void testGetMoveQueenRight() {
        ChessBoard chessBoard = new ChessBoard();
        Queen queen = new Queen(Color.B, new Position(3, 0), chessBoard.getBoard());
        queen.getMovesRight();
        List<Position> actual = queen.getPossiblesMovements();

        assertEquals(7, actual.size());
    }

    @Test
    public void testGetMoveQueenLeft() {
        ChessBoard chessBoard = new ChessBoard();
        Queen queen = new Queen(Color.B, new Position(2, 3), chessBoard.getBoard());
        queen.getMovesLeft();
        List<Position> actual = queen.getPossiblesMovements();

        assertEquals(3, actual.size());
    }

    @Test
    public void testGetMoveQueenUp() {
        ChessBoard chessBoard = new ChessBoard();
        Queen queen = new Queen(Color.B, new Position(4, 0), chessBoard.getBoard());
        queen.getMovesUp();
        List<Position> actual = queen.getPossiblesMovements();

        assertEquals(2, actual.size());
    }

    @Test
    public void testGetMoveQueenDown() {
        ChessBoard chessBoard = new ChessBoard();
        Queen queen = new Queen(Color.B, new Position(2, 0), chessBoard.getBoard());
        queen.getMovesDown();
        List<Position> actual = queen.getPossiblesMovements();
        assertEquals(4, actual.size());
    }

    @Test
    public void testGetMoveQueenTopRight() {
        ChessBoard chessBoard = new ChessBoard();
        Queen queen = new Queen(Color.B, new Position(2, 0), chessBoard.getBoard());
        queen.getMovesTopRight();
        List<Position> actual = queen.getPossiblesMovements();

        assertEquals(0, actual.size());
    }

    @Test
    public void testGetMoveQueenTopLeft() {
        ChessBoard chessBoard = new ChessBoard();
        Queen queen = new Queen(Color.B, new Position(2, 0), chessBoard.getBoard());
        queen.getMovesTopLeft();
        List<Position> actual = queen.getPossiblesMovements();

        assertEquals(0, actual.size());
    }

    @Test
    public void testGetMoveQueenBottomLeft() {
        ChessBoard chessBoard = new ChessBoard();
        Queen queen = new Queen(Color.B, new Position(2, 0), chessBoard.getBoard());
        queen.getMovesBottomLeft();
        List<Position> actual = queen.getPossiblesMovements();

        assertEquals(0, actual.size());
    }

    @Test
    public void testGetMoveQueenBottomRight() {
        ChessBoard chessBoard = new ChessBoard();
        Queen queen = new Queen(Color.B, new Position(2, 0), chessBoard.getBoard());
        queen.getMovesBottomRight();
        List<Position> actual = queen.getPossiblesMovements();

        assertEquals(4, actual.size());
    }

    @Test
    public void testQueenIsTheSameColor() {
        Piece[][] board = new Piece[8][8];
        Piece piece = new Queen(Color.W, new Position(2,3), board);
        Queen queen = new Queen(Color.B, new Position(2, 0), board);
        assertFalse(queen.isSameColor(piece));
    }

    @Test
    public void testQueenIsPossibleMove() {
        ChessBoard chessBoard = new ChessBoard();
        Queen queen = new Queen(Color.B, new Position(2, 0), chessBoard.getBoard());
        assertTrue(queen.isPossibleMove(new Position(3,0)));
    }
}