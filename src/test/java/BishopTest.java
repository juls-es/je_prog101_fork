import org.junit.Test;

import static org.junit.Assert.*;

public class BishopTest {
    @Test
    public void destroyPieceTest() {
        Piece[][] board = new Piece[8][8];
        Bishop bishop = new Bishop(Color.W, new Position(0, 0), board);
        Piece piece = new Bishop(Color.W, new Position(0, 0), board);
        boolean expected = false;
        bishop.destroyPiece(piece);
        boolean actual = piece.doesExist();
        assertEquals(expected, actual);
    }

    @Test
    public void diePieceTest() {
        Piece[][] board = new Piece[8][8];
        Bishop bishop = new Bishop(Color.W, new Position(0, 0), board);
        boolean expected = false;
        bishop.die();
        boolean actual = bishop.doesExist();
        assertEquals(expected, actual);
    }

    @Test
    public void isPosibleMoveToAPositionTest() {
        Piece[][] board = new Piece[8][8];
        Bishop bishop = new Bishop(Color.W, new Position(0, 0), board);
        boolean expected = true;
        boolean actual = bishop.isPossibleMove(new Position(1, 1));
        assertEquals(expected, actual);
    }

    @Test
    public void isNotPosibleMoveToPositionWhenThereIsAOtherPieceTest() {
        Piece[][] board = new Piece[8][8];
        board[1][1] = new Bishop(Color.W, new Position(1, 1), board);
        Bishop bishop = new Bishop(Color.W, new Position(0, 0), board);

        boolean expected = false;
        boolean actual = bishop.isPossibleMove(new Position(1, 1));
        assertEquals(expected, actual);
    }

    @Test
    public void isPosibleMoveToUpLeftPositionTest() {
        Piece[][] board = new Piece[8][8];
        Bishop bishop = new Bishop(Color.W, new Position(2, 2), board);
        boolean expected = true;
        boolean actual = bishop.isPossibleMove(new Position(1, 1));
        assertEquals(expected, actual);
    }

    @Test
    public void isNotPosibleMoveToUpLeftPositionTest() {
        Piece[][] board = new Piece[8][8];
        board[1][1] = new Bishop(Color.W, new Position(1, 1), board);
        Bishop bishop = new Bishop(Color.W, new Position(2, 2), board);

        boolean expected = false;
        boolean actual = bishop.isPossibleMove(new Position(0, 0));
        assertEquals(expected, actual);
    }

    @Test
    public void isPosibleMoveToUpRightPositionTest() {
        Piece[][] board = new Piece[8][8];
        Bishop bishop = new Bishop(Color.W, new Position(2, 2), board);
        boolean expected = true;
        boolean actual = bishop.isPossibleMove(new Position(0, 4));
        assertEquals(expected, actual);
    }

    @Test
    public void isNotPosibleMoveToUpRightPositionTest() {
        Piece[][] board = new Piece[8][8];
        board[1][1] = new Bishop(Color.W, new Position(1, 1), board);
        Bishop bishop = new Bishop(Color.W, new Position(2, 2), board);

        boolean expected = true;
        boolean actual = bishop.isPossibleMove(new Position(0, 4));
        assertEquals(expected, actual);
    }

    @Test
    public void isPosibleMoveToDownLeftPositionTest() {
        Piece[][] board = new Piece[8][8];
        Bishop bishop = new Bishop(Color.W, new Position(5, 5), board);
        boolean expected = true;
        boolean actual = bishop.isPossibleMove(new Position(7, 3));
        assertEquals(expected, actual);
    }

    @Test
    public void isNotPosibleMoveToDownLeftPositionTest() {
        Piece[][] board = new Piece[8][8];
        board[1][1] = new Bishop(Color.W, new Position(1, 1), board);
        Bishop bishop = new Bishop(Color.W, new Position(5, 5), board);

        boolean expected = true;
        boolean actual = bishop.isPossibleMove(new Position(7, 3));
        assertEquals(expected, actual);
    }

    @Test
    public void isPosibleMoveToDownRightPositionTest() {
        Piece[][] board = new Piece[8][8];
        Bishop bishop = new Bishop(Color.W, new Position(5, 5), board);
        boolean expected = true;
        boolean actual = bishop.isPossibleMove(new Position(7, 7));
        assertEquals(expected, actual);
    }

    @Test
    public void isNotPosibleMoveToDownRightPositionTest() {
        Piece[][] board = new Piece[8][8];
        board[1][1] = new Bishop(Color.W, new Position(1, 1), board);
        Bishop bishop = new Bishop(Color.W, new Position(5, 5), board);

        boolean expected = true;
        boolean actual = bishop.isPossibleMove(new Position(7, 7));
        assertEquals(expected, actual);
    }

    @Test
    public void isNotPosibleMoveAndDestroyAPieceTest() {
        Piece[][] board = new Piece[8][8];
        Bishop bishopDie = new Bishop(Color.W, new Position(6, 6), board);
        board[6][6] = new Bishop(Color.W, new Position(6, 6), board);
        Bishop bishop = new Bishop(Color.W, new Position(5, 5), board);

        boolean expected = true;
        bishop.move(new Position(6, 6));
        boolean actual = bishopDie.doesExist();
        assertEquals(expected, actual);
    }
}