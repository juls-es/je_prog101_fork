import org.junit.Test;

import static org.junit.Assert.*;
public class CheckKingTest {
    @Test
    public void testThatABishopOfDifferentColorCanCheck() {
        Piece[][] board = new Piece[8][8];
        King king = new King(Color.B, new Position(3,3), board);
        Bishop bishop = new Bishop(Color.W, new Position(0, 6), board);
        board[3][3] = king;
        board[0][6] = bishop;
        assertTrue(king.isTheKingInCheck());
    }
    @Test
    public void testThatARookOfDifferentColorCanCheck() {
        Piece[][] board = new Piece[8][8];
        King king = new King(Color.B, new Position(3,3), board);
        Rook rook = new Rook(Color.W, new Position(0, 3), board);
        board[3][3] = king;
        board[0][3] = rook;
        assertTrue(king.isTheKingInCheck());
    }
    @Test
    public void testThatAQueenOfDifferentColorCanCheck() {
        Piece[][] board = new Piece[8][8];
        King king = new King(Color.B, new Position(3,3), board);
        Queen queen = new Queen(Color.W, new Position(0, 6), board);
        board[3][3] = king;
        board[0][6] = queen;
        assertTrue(king.isTheKingInCheck());
    }

    @Test
    public void testThatABishopOfSameColorCanNotCheck() {
        Piece[][] board = new Piece[8][8];
        King king = new King(Color.B, new Position(3,3), board);
        Bishop bishop = new Bishop(Color.B, new Position(0, 6), board);
        board[3][3] = king;
        board[0][6] = bishop;
        assertFalse(king.isTheKingInCheck());
    }
    @Test
    public void testThatARookOfSameColorCanNotCheck() {
        Piece[][] board = new Piece[8][8];
        King king = new King(Color.W, new Position(3,3), board);
        Rook rook = new Rook(Color.W, new Position(0, 3), board);
        board[3][3] = king;
        board[0][3] = rook;
        assertFalse(king.isTheKingInCheck());
    }
    @Test
    public void testThatAQueenOfSameColorCanNotCheck() {
        Piece[][] board = new Piece[8][8];
        King king = new King(Color.B, new Position(3,3), board);
        Queen queen = new Queen(Color.B, new Position(0, 6), board);
        board[3][3] = king;
        board[0][6] = queen;
        assertFalse(king.isTheKingInCheck());
    }
    @Test
    public void testThatAQueenCanCheckInUp() {
        Piece[][] board = new Piece[8][8];
        King king = new King(Color.B, new Position(3,3), board);
        Queen queen = new Queen(Color.W, new Position(6, 3), board);
        board[3][3] = king;
        board[6][3] = queen;
        assertTrue(king.isTheKingInCheck());
    }
    @Test
    public void testThatAQueenCanCheckInDown() {
        Piece[][] board = new Piece[8][8];
        King king = new King(Color.B, new Position(3,3), board);
        Queen queen = new Queen(Color.W, new Position(0, 3), board);
        board[3][3] = king;
        board[0][3] = queen;
        assertTrue(king.isTheKingInCheck());
    }
    @Test
    public void testThatAQueenCanCheckInRight() {
        Piece[][] board = new Piece[8][8];
        King king = new King(Color.B, new Position(3,3), board);
        Queen queen = new Queen(Color.W, new Position(3, 6), board);
        board[3][3] = king;
        board[3][6] = queen;
        assertTrue(king.isTheKingInCheck());
    }
    @Test
    public void testThatAQueenCanCheckInLeft() {
        Piece[][] board = new Piece[8][8];
        King king = new King(Color.B, new Position(3,3), board);
        Queen queen = new Queen(Color.W, new Position(3, 1), board);
        board[3][3] = king;
        board[3][1] = queen;
        assertTrue(king.isTheKingInCheck());
    }
    @Test
    public void testThatAQueenCanCheckInTopRight() {
        Piece[][] board = new Piece[8][8];
        King king = new King(Color.B, new Position(3,3), board);
        Queen queen = new Queen(Color.W, new Position(1, 5), board);
        board[3][3] = king;
        board[1][5] = queen;
        assertTrue(king.isTheKingInCheck());
    }
    @Test
    public void testThatAQueenCanCheckInTopLeft() {
        Piece[][] board = new Piece[8][8];
        King king = new King(Color.B, new Position(3,3), board);
        Queen queen = new Queen(Color.W, new Position(1, 1), board);
        board[3][3] = king;
        board[1][1] = queen;
        assertTrue(king.isTheKingInCheck());
    }
    @Test
    public void testThatAQueenCanCheckInBottomRight() {
        Piece[][] board = new Piece[8][8];
        King king = new King(Color.B, new Position(3,3), board);
        Queen queen = new Queen(Color.W, new Position(6, 6), board);
        board[3][3] = king;
        board[6][6] = queen;
        assertTrue(king.isTheKingInCheck());
    }
    @Test
    public void testThatAQueenCanCheckInBottomLeft() {
        Piece[][] board = new Piece[8][8];
        King king = new King(Color.B, new Position(3,3), board);
        Queen queen = new Queen(Color.W, new Position(5, 1), board);
        board[3][3] = king;
        board[5][1] = queen;
        assertTrue(king.isTheKingInCheck());
    }

    @Test
    public void testThatABishopCanCheckInTopRight() {
        Piece[][] board = new Piece[8][8];
        King king = new King(Color.W, new Position(4,2), board);
        Bishop bishop = new Bishop(Color.B, new Position(2, 4), board);
        board[4][2] = king;
        board[2][4] = bishop;
        assertTrue(king.isTheKingInCheck());
    }
    @Test
    public void testThatABishopCanCheckInTopLeft() {
        Piece[][] board = new Piece[8][8];
        King king = new King(Color.W, new Position(4,2), board);
        Bishop bishop = new Bishop(Color.B, new Position(2, 0), board);
        board[4][2] = king;
        board[2][0] = bishop;
        assertTrue(king.isTheKingInCheck());
    }
    @Test
    public void testThatABishopCanCheckInBottomRight() {
        Piece[][] board = new Piece[8][8];
        King king = new King(Color.W, new Position(4,2), board);
        Bishop bishop = new Bishop(Color.B, new Position(6, 4), board);
        board[4][2] = king;
        board[6][4] = bishop;
        assertTrue(king.isTheKingInCheck());
    }
    @Test
    public void testThatABishopCanCheckInBottomLeft() {
        Piece[][] board = new Piece[8][8];
        King king = new King(Color.W, new Position(4,2), board);
        Bishop bishop = new Bishop(Color.B, new Position(5, 1), board);
        board[4][2] = king;
        board[5][1] = bishop;
        assertTrue(king.isTheKingInCheck());
    }

    @Test
    public void testThatARookCanCheckInUp() {
        Piece[][] board = new Piece[8][8];
        King king = new King(Color.W, new Position(5,5), board);
        Rook rook = new Rook(Color.B, new Position(7, 5), board);
        board[5][5] = king;
        board[7][5] = rook;
        assertTrue(king.isTheKingInCheck());
    }
    @Test
    public void testThatARookCanCheckInDown() {
        Piece[][] board = new Piece[8][8];
        King king = new King(Color.W, new Position(5,5), board);
        Rook rook = new Rook(Color.B, new Position(1, 5), board);
        board[5][5] = king;
        board[1][5] = rook;
        assertTrue(king.isTheKingInCheck());
    }
    @Test
    public void testThatARookCanCheckInRight() {
        Piece[][] board = new Piece[8][8];
        King king = new King(Color.W, new Position(5,5), board);
        Rook rook = new Rook(Color.B, new Position(5, 2), board);
        board[5][5] = king;
        board[5][2] = rook;
        assertTrue(king.isTheKingInCheck());
    }
    @Test
    public void testThatARookCanCheckInLeft() {
        Piece[][] board = new Piece[8][8];
        King king = new King(Color.W, new Position(5,5), board);
        Rook rook = new Rook(Color.B, new Position(5, 7), board);
        board[5][5] = king;
        board[5][7] = rook;
        assertTrue(king.isTheKingInCheck());
    }

    @Test
    public void testThatAHorseCanCheckIfItIsInL() {
        Piece[][] board = new Piece[8][8];
        King king = new King(Color.W, new Position(3, 4), board);
        board[3][4] = king;
        Horse horse = new Horse(Color.B, new Position(4, 2), board);
        board[4][2] = horse;
        assertTrue(king.isTheKingInCheck());
    }
    @Test
    public void testThatAHorseSameColorCanNotCheckIfItIsInL() {
        Piece[][] board = new Piece[8][8];
        King king = new King(Color.W, new Position(3, 4), board);
        board[3][4] = king;
        Horse horse = new Horse(Color.W, new Position(4, 2), board);
        board[4][2] = horse;
        assertFalse(king.isTheKingInCheck());
    }

    @Test
    public void testThatAHorseCanNotCheckIfItIsNotInL() {
        Piece[][] board = new Piece[8][8];
        King king = new King(Color.W, new Position(3, 4), board);
        board[3][4] = king;
        Horse horse = new Horse(Color.B, new Position(3, 2), board);
        board[3][2] = horse;
        assertFalse(king.isTheKingInCheck());
    }

    @Test
    public void testThatAHorseCanCheckIfIHasDifferentColor() {
        Piece[][] board = new Piece[8][8];
        King king = new King(Color.B, new Position(3, 4), board);
        board[3][4] = king;
        Horse horse = new Horse(Color.W, new Position(1, 5), board);
        board[1][5] = horse;
        assertTrue(king.isTheKingInCheck());
    }

    @Test
    public void testFromRightDiagonalTheBlackPawnCanCheckToWhiteKing() {
        Piece[][] board = new Piece[8][8];
        King king = new King(Color.W, new Position(3, 1), board);
        board[3][1] = king;
        Pawn pawn = new Pawn(Color.B, new Position(2, 2), board);
        board[2][2] = pawn;
        assertTrue(king.isTheKingInCheck());
    }
    @Test
    public void testFromLeftDiagonalTheBlackPawnCanCheckToWhiteKing() {
        Piece[][] board = new Piece[8][8];
        King king = new King(Color.W, new Position(3, 1), board);
        board[3][1] = king;
        Pawn pawn = new Pawn(Color.B, new Position(2, 0), board);
        board[2][0] = pawn;
        assertTrue(king.isTheKingInCheck());
    }
    @Test
    public void testFromRightDiagonalTheBlackPawnCanNotCheckToBlackKing() {
        Piece[][] board = new Piece[8][8];
        King king = new King(Color.B, new Position(3, 1), board);
        board[3][1] = king;
        Pawn pawn = new Pawn(Color.B, new Position(2, 2), board);
        board[2][2] = pawn;
        assertFalse(king.isTheKingInCheck());
    }
    @Test
    public void testFromLeftDiagonalTheBlackPawnCanNotCheckToBlackKing() {
        Piece[][] board = new Piece[8][8];
        King king = new King(Color.B, new Position(3, 1), board);
        board[3][1] = king;
        Pawn pawn = new Pawn(Color.B, new Position(2, 0), board);
        board[2][0] = pawn;
        assertFalse(king.isTheKingInCheck());
    }
    @Test
    public void testFromRightDiagonalTheWhitePawnCanCheckToBlackKing() {
        Piece[][] board = new Piece[8][8];
        King king = new King(Color.B, new Position(3, 1), board);
        board[3][1] = king;
        Pawn pawn = new Pawn(Color.W, new Position(4, 2), board);
        board[4][2] = pawn;
        assertTrue(king.isTheKingInCheck());
    }
    @Test
    public void testFromLeftDiagonalTheWhitePawnCanCheckToBlackKing() {
        Piece[][] board = new Piece[8][8];
        King king = new King(Color.B, new Position(3, 1), board);
        board[3][1] = king;
        Pawn pawn = new Pawn(Color.W, new Position(4, 0), board);
        board[4][0] = pawn;
        assertTrue(king.isTheKingInCheck());
    }
    @Test
    public void testFromRightDiagonalTheWhitePawnCanNotCheckToWhiteKing() {
        Piece[][] board = new Piece[8][8];
        King king = new King(Color.W, new Position(3, 1), board);
        board[3][1] = king;
        Pawn pawn = new Pawn(Color.W, new Position(4, 2), board);
        board[4][2] = pawn;
        assertFalse(king.isTheKingInCheck());
    }
    @Test
    public void testFromLeftDiagonalTheBlackPawnCanNotCheckToWhiteKing() {
        Piece[][] board = new Piece[8][8];
        King king = new King(Color.W, new Position(3, 1), board);
        board[3][1] = king;
        Pawn pawn = new Pawn(Color.W, new Position(4, 0), board);
        board[4][0] = pawn;
        assertFalse(king.isTheKingInCheck());
    }

    @Test
    public void testBishopNotCheckingIfThereArePieceInTheMiddle() {
        Piece[][] board = new Piece[8][8];
        King king = new King(Color.B, new Position(0, 4), board);
        board[0][4] = king;
        Pawn pawn = new Pawn(Color.B, new Position(2, 6), board);
        board[2][6] = pawn;
        Bishop bishop = new Bishop(Color.W, new Position(3, 7), board);
        board[3][7] = bishop;
        assertFalse(king.isTheKingInCheck());
    }
    @Test
    public void testQueenNotCheckingIfThereArePieceInTheMiddle() {
        Piece[][] board = new Piece[8][8];
        King king = new King(Color.B, new Position(0, 4), board);
        board[0][4] = king;
        Pawn pawn = new Pawn(Color.B, new Position(2, 4), board);
        board[2][4] = pawn;
        Queen queen = new Queen(Color.W, new Position(5, 4), board);
        board[5][4] = queen;
        assertFalse(king.isTheKingInCheck());
    }
    @Test
    public void testRookCheckingIfThereArePieceInTheMiddle() {
        Piece[][] board = new Piece[8][8];
        King king = new King(Color.B, new Position(4, 4), board);
        board[4][4] = king;
        Pawn pawn = new Pawn(Color.B, new Position(1, 4), board);
        board[1][4] = pawn;
        Rook rook = new Rook(Color.W, new Position(0, 4), board);
        board[0][4] = rook;
        assertFalse(king.isTheKingInCheck());
    }
    @Test
    public void testIfNotCheckWhenThereIsAPiece(){
        Piece[][] board = new Piece[8][8];
        King king = new King(Color.B, new Position(0, 4), board);
        board[0][4] = king;
        Rook rook = new Rook(Color.B, new Position(0, 1), board);
        board[0][1] = rook;
        Bishop bishop = new Bishop(Color.B, new Position(0, 2), board);
        board[0][2] = bishop;
        assertFalse(king.isTheKingInCheck());
    }
    @Test
    public void testIfChekWhenAPosition(){
        Piece[][] board = new Piece[8][8];
        King king = new King(Color.B, new Position(0, 4), board);
        board[0][4] = king;
        Rook rook = new Rook(Color.B, new Position(0, 1), board);
        board[0][1] = rook;
        Bishop bishop = new Bishop(Color.B, new Position(0, 2), board);
        board[0][2] = bishop;
        assertFalse(king.isTheKingInCheck(new Position(0, 4)));
    }

    @Test
    public void testIfChekPawm(){
        Piece[][] board = new Piece[8][8];
        King king = new King(Color.B, new Position(0, 4), board);
        board[0][4] = king;
        Pawn pawn = new Pawn(Color.W, new Position(1, 3), board);
        board[1][3] = pawn;
        assertTrue(king.isTheKingInCheck());
    }
    @Test
    public void testIfChekPawmBlack(){
        Piece[][] board = new Piece[8][8];
        King king = new King(Color.W, new Position(1, 3), board);
        board[1][3] = king;
        Pawn pawn = new Pawn(Color.B, new Position(0, 2), board);
        board[0][2] = pawn;
        assertTrue(king.isTheKingInCheck());
    }
}
