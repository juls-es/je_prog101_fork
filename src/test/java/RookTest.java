import org.junit.Test;


import java.util.List;

import static org.junit.Assert.*;

public class RookTest {
    @Test
    public void testGetMoveRookRight() {
        ChessBoard chessBoard = new ChessBoard();
        Rook rook = new Rook(Color.B, new Position(3, 0), chessBoard.getBoard());
        rook. getMovesRight();
        List<Position> actual = rook. getPossiblesMovements();

        assertEquals(7, actual.size());
    }

    @Test
    public void testGetMoveRookLeft() {
        ChessBoard chessBoard = new ChessBoard();
        Rook rook = new Rook(Color.B, new Position(2, 3), chessBoard.getBoard());
        rook. getMovesLeft();
        List<Position> actual = rook. getPossiblesMovements();

        assertEquals(3, actual.size());
    }

    @Test
    public void testGetMoveRookUp() {
        ChessBoard chessBoard = new ChessBoard();
        Rook rook = new Rook(Color.B, new Position(4, 0), chessBoard.getBoard());
        rook. getMovesUp();
        List<Position> actual = rook.getPossiblesMovements();

        assertEquals(2, actual.size());
    }

    @Test
    public void testGetMoveRookDown() {
        ChessBoard chessBoard = new ChessBoard();
        Rook rook = new Rook(Color.B, new Position(2, 0), chessBoard.getBoard());
        rook. getMovesDown();
        List<Position> actual = rook. getPossiblesMovements();
        assertEquals(4, actual.size());
    }

    @Test
    public void testRookIsTheSameColor() {
        Piece[][] board = new Piece[8][8];
        Piece piece = new Rook(Color.W, new Position(2,3), board);
        Rook rook = new Rook(Color.B, new Position(2, 0), board);
        assertFalse(rook.isSameColor(piece));
    }

    @Test
    public void testRookIsPossibleMove() {
        ChessBoard chessBoard = new ChessBoard();
        Rook rook = new Rook(Color.B, new Position(2, 0), chessBoard.getBoard());
        assertTrue(rook.isPossibleMove(new Position(3,0)));
    }

    @Test
    public void testRookIsPossibleDestroyAPiece() {
        ChessBoard chessBoard = new ChessBoard();
        Rook rook = new Rook(Color.B, new Position(2, 0), chessBoard.getBoard());
        rook.getPossibleMoves();
        rook.destroy(rook.getPiece((new Position(6, 0))));
        assertTrue(rook.doesExistPiece(new Position(6,0)));
    }

    @Test
    public void testRookIsPossibleMoveRook() {
        ChessBoard chessBoard = new ChessBoard();
        Rook rook = (Rook) chessBoard.getPiece(new Position(0, 0));
        //rook.getPossibleMoves();
        rook.move(new Position(6, 0));
        assertEquals(0, rook.getPosition().getRow());
    }
}
