import org.junit.Test;

import static org.junit.Assert.*;
public class PromotableTest {
    @Test
    public void testReturnTrueIfWhitePawnIsPromotedToBishop() {
        Piece[][] board = new Piece[8][8];
        Pawn pawn = new Pawn(Color.W, new Position(1,6), board);
        board[1][5] = pawn;
        pawn.promotion(new Position(0,6), "B");
        assertTrue(board[0][6] instanceof Bishop);
    }
    @Test
    public void testReturnTrueIfWhitePawnIsPromotedToHorse() {
        Piece[][] board = new Piece[8][8];
        Pawn pawn = new Pawn(Color.W, new Position(1,6), board);
        board[1][5] = pawn;
        pawn.promotion(new Position(0,6), "H");
        assertTrue(board[0][6] instanceof Horse);
    }
    @Test
    public void testReturnTrueIfWhitePawnIsPromotedToRook() {
        Piece[][] board = new Piece[8][8];
        Pawn pawn = new Pawn(Color.W, new Position(1,6), board);
        board[1][5] = pawn;
        pawn.promotion(new Position(0,6), "R");
        assertTrue(board[0][6] instanceof Rook);
    }
    @Test
    public void testReturnTrueIfWhitePawnIsPromotedToQueen() {
        Piece[][] board = new Piece[8][8];
        Pawn pawn = new Pawn(Color.W, new Position(1,6), board);
        board[1][5] = pawn;
        pawn.promotion(new Position(0,6), "Q");
        assertTrue(board[0][6] instanceof Queen);
    }
}
