import org.junit.Test;

import static org.junit.Assert.*;

public class PawnTest {
    @Test
    public void testPossibleBlackPawnMovesTwoWhenIsInitialPosition() {
        Piece[][] board = new Piece[8][8];
        Pawn pawn = new Pawn(Color.B, new Position(1,5), board);
        board[1][5] = pawn;
        assertEquals(2, pawn.getPossibleMoves().size());
    }
    @Test
    public void testPossibleBlackPawnMovesOneWhenIsInitialPositionAndSecondPosOccupiedSameColor() {
        Piece[][] board = new Piece[8][8];
        Pawn pawn = new Pawn(Color.B, new Position(1,5), board);
        Pawn pawn2 = new Pawn(Color.B, new Position(3,5), board);
        board[1][5] = pawn;
        board[3][5] = pawn2;
        assertEquals(1, pawn.getPossibleMoves().size());
    }
    @Test
    public void testPossibleBlackPawnMovesTwoWhenIsInitialPositionAndSecondPosOccupiedDifferentColor() {
        Piece[][] board = new Piece[8][8];
        Pawn pawn = new Pawn(Color.B, new Position(1,5), board);
        Pawn pawn2 = new Pawn(Color.W, new Position(3,5), board);
        board[1][5] = pawn;
        board[3][5] = pawn2;
        assertEquals(2, pawn.getPossibleMoves().size());
    }
    @Test
    public void testPossibleBlackPawnNotMovesWhenIsInitialPositionAndSecondPosOccupiedSameColor() {
        Piece[][] board = new Piece[8][8];
        Pawn pawn = new Pawn(Color.B, new Position(1,5), board);
        Pawn pawn2 = new Pawn(Color.B, new Position(2,5), board);
        board[1][5] = pawn;
        board[2][5] = pawn2;
        assertEquals(0, pawn.getPossibleMoves().size());
    }

    @Test
    public void testPossibleBlackPawnMovesThreeIfPieceInDiagonalLeftDifferentColor() {
        Piece[][] board = new Piece[8][8];
        Pawn pawn = new Pawn(Color.B, new Position(1,5), board);
        Pawn pawn2 = new Pawn(Color.W, new Position(2,4), board);
        board[1][5] = pawn;
        board[2][4] =pawn2;
        assertEquals(3, pawn.getPossibleMoves().size());
    }
    @Test
    public void testPossibleBlackPawnMovesThreeIfPieceInDiagonalRightDifferentColor() {
        Piece[][] board = new Piece[8][8];
        Pawn pawn = new Pawn(Color.B, new Position(1,5), board);
        Pawn pawn2 = new Pawn(Color.W, new Position(2,6), board);
        board[1][5] = pawn;
        board[2][6] =pawn2;
        assertEquals(3, pawn.getPossibleMoves().size());
    }
    @Test
    public void testPossibleBlackPawnMovesFourIfPieceInDiagonalRightAndLeftDifferentColor() {
        Piece[][] board = new Piece[8][8];
        Pawn pawn = new Pawn(Color.B, new Position(1,5), board);
        Pawn pawn2 = new Pawn(Color.W, new Position(2,6), board);
        Pawn pawn3 = new Pawn(Color.W, new Position(2,4), board);
        board[1][5] = pawn;
        board[2][6] =pawn2;
        board[2][4] =pawn3;
        assertEquals(4, pawn.getPossibleMoves().size());
    }

    @Test
    public void testPossibleBlackPawnMovesTwoIfPieceInDiagonalLeftSameColor() {
        Piece[][] board = new Piece[8][8];
        Pawn pawn = new Pawn(Color.B, new Position(1,5), board);
        Pawn pawn2 = new Pawn(Color.B, new Position(2,4), board);
        board[1][5] = pawn;
        board[2][4] =pawn2;
        assertEquals(2, pawn.getPossibleMoves().size());
    }
    @Test
    public void testPossibleBlackPawnMovesTwoIfPieceInDiagonalRightSameColor() {
        Piece[][] board = new Piece[8][8];
        Pawn pawn = new Pawn(Color.B, new Position(1,5), board);
        Pawn pawn2 = new Pawn(Color.B, new Position(2,6), board);
        board[1][5] = pawn;
        board[2][6] =pawn2;
        assertEquals(2, pawn.getPossibleMoves().size());
    }
    @Test
    public void testPossibleBlackPawnMovesTwoIfPieceInDiagonalRightAndLeftSameColor() {
        Piece[][] board = new Piece[8][8];
        Pawn pawn = new Pawn(Color.B, new Position(1,5), board);
        Pawn pawn2 = new Pawn(Color.B, new Position(2,6), board);
        Pawn pawn3 = new Pawn(Color.B, new Position(2,4), board);
        board[1][5] = pawn;
        board[2][6] =pawn2;
        board[2][4] =pawn3;
        assertEquals(2, pawn.getPossibleMoves().size());
    }
    @Test
    public void testPossibleBlackPawnMovesFourIfPieceInDiagonalRightAndLeftSameColor() {
        Piece[][] board = new Piece[8][8];
        Pawn pawn = new Pawn(Color.B, new Position(1,5), board);
        Pawn pawn2 = new Pawn(Color.W, new Position(2,6), board);
        Pawn pawn3 = new Pawn(Color.W, new Position(2,4), board);
        board[1][5] = pawn;
        board[2][6] =pawn2;
        board[2][4] =pawn3;
        assertEquals(4, pawn.getPossibleMoves().size());
    }

    @Test
    public void testPossibleWhitePawnMovesTwoWhenIsInitialPosition() {
        Piece[][] board = new Piece[8][8];
        Pawn pawn = new Pawn(Color.W, new Position(6,5), board);
        board[6][5] = pawn;
        assertEquals(2, pawn.getPossibleMoves().size());
    }
    @Test
    public void testPossibleWhitePawnMovesOneWhenIsInitialPositionAndSecondPosOccupiedSameColor() {
        Piece[][] board = new Piece[8][8];
        Pawn pawn = new Pawn(Color.W, new Position(6,5), board);
        Pawn pawn2 = new Pawn(Color.W, new Position(4,5), board);
        board[6][5] = pawn;
        board[4][5] = pawn2;
        assertEquals(1, pawn.getPossibleMoves().size());
    }
    @Test
    public void testPossibleWhitePawnMovesTwoWhenIsInitialPositionAndSecondPosOccupiedDifferentColor() {
        Piece[][] board = new Piece[8][8];
        Pawn pawn = new Pawn(Color.W, new Position(6,5), board);
        Pawn pawn2 = new Pawn(Color.B, new Position(4,5), board);
        board[6][5] = pawn;
        board[4][5] = pawn2;
        assertEquals(2, pawn.getPossibleMoves().size());
    }
    @Test
    public void testPossibleWhitePawnNotMovesWhenIsInitialPositionAndSecondPosOccupiedSameColor() {
        Piece[][] board = new Piece[8][8];
        Pawn pawn = new Pawn(Color.W, new Position(6,5), board);
        Pawn pawn2 = new Pawn(Color.W, new Position(5,5), board);
        board[6][5] = pawn;
        board[5][5] = pawn2;
        assertEquals(0, pawn.getPossibleMoves().size());
    }

    @Test
    public void testPossibleWhitePawnMovesThreeIfPieceInDiagonalLeftDifferentColor() {
        Piece[][] board = new Piece[8][8];
        Pawn pawn = new Pawn(Color.W, new Position(6,5), board);
        Pawn pawn2 = new Pawn(Color.B, new Position(5,4), board);
        board[6][5] = pawn;
        board[5][4] =pawn2;
        assertEquals(3, pawn.getPossibleMoves().size());
    }
    @Test
    public void testPossibleWhitePawnMovesThreeIfPieceInDiagonalRightDifferentColor() {
        Piece[][] board = new Piece[8][8];
        Pawn pawn = new Pawn(Color.W, new Position(6,5), board);
        Pawn pawn2 = new Pawn(Color.B, new Position(5,6), board);
        board[1][5] = pawn;
        board[5][6] =pawn2;
        assertEquals(3, pawn.getPossibleMoves().size());
    }
    @Test
    public void testPossibleWhitePawnMovesFourIfPieceInDiagonalRightAndLeftDifferentColor() {
        Piece[][] board = new Piece[8][8];
        Pawn pawn = new Pawn(Color.W, new Position(6,5), board);
        Pawn pawn2 = new Pawn(Color.B, new Position(5,4), board);
        Pawn pawn3 = new Pawn(Color.B, new Position(5,6), board);
        board[6][5] = pawn;
        board[5][4] =pawn2;
        board[5][6] =pawn3;
        assertEquals(4, pawn.getPossibleMoves().size());
    }

    @Test
    public void testPossibleWhitePawnMovesTwoIfPieceInDiagonalLeftSameColor() {
        Piece[][] board = new Piece[8][8];
        Pawn pawn = new Pawn(Color.W, new Position(6,5), board);
        Pawn pawn2 = new Pawn(Color.W, new Position(5,4), board);
        board[6][5] = pawn;
        board[5][4] =pawn2;
        assertEquals(2, pawn.getPossibleMoves().size());
    }
    @Test
    public void testPossibleWhitePawnMovesTwoIfPieceInDiagonalRightSameColor() {
        Piece[][] board = new Piece[8][8];
        Pawn pawn = new Pawn(Color.W, new Position(6,5), board);
        Pawn pawn2 = new Pawn(Color.W, new Position(5,6), board);
        board[6][5] = pawn;
        board[5][6] =pawn2;
        assertEquals(2, pawn.getPossibleMoves().size());
    }
    @Test
    public void testPossibleWhitePawnMovesTwoIfPieceInDiagonalRightAndLeftSameColor() {
        Piece[][] board = new Piece[8][8];
        Pawn pawn = new Pawn(Color.W, new Position(6,5), board);
        Pawn pawn2 = new Pawn(Color.W, new Position(5,6), board);
        Pawn pawn3 = new Pawn(Color.W, new Position(5,4), board);
        board[6][5] = pawn;
        board[5][6] =pawn2;
        board[5][4] =pawn3;
        assertEquals(2, pawn.getPossibleMoves().size());
    }
    @Test
    public void testPossibleWhitePawnMovesFourIfPieceInDiagonalRightAndLeftSameColor() {
        Piece[][] board = new Piece[8][8];
        Pawn pawn = new Pawn(Color.W, new Position(6,5), board);
        Pawn pawn2 = new Pawn(Color.B, new Position(5,6), board);
        Pawn pawn3 = new Pawn(Color.B, new Position(5,4), board);
        board[6][5] = pawn;
        board[5][6] =pawn2;
        board[5][4] =pawn3;
        assertEquals(4, pawn.getPossibleMoves().size());
    }

    @Test
    public void testWhenIsNotInitPositionInitialAndDiagonalsAreOccupiedByDifferentColors() {
        Piece[][] board = new Piece[8][8];
        Pawn pawn = new Pawn(Color.W, new Position(3,4), board);
        pawn.setDoesMoved(true);
        Pawn pawn2 = new Pawn(Color.B, new Position(2,3), board);
        Pawn pawn3 = new Pawn(Color.B, new Position(2,5), board);
        board[3][4] = pawn;
        board[2][3] =pawn2;
        board[2][5] =pawn3;
        assertEquals(3, pawn.getPossibleMoves().size());
    }

    @Test
    public void testWhenIsNotInitPositionInitialAndOneDiagonalsIsOccupiedByDifferentColor() {
        Piece[][] board = new Piece[8][8];
        Pawn pawn = new Pawn(Color.W, new Position(3,4), board);
        pawn.setDoesMoved(true);
        Pawn pawn2 = new Pawn(Color.B, new Position(2,3), board);
        Pawn pawn3 = new Pawn(Color.W, new Position(2,5), board);
        board[3][4] = pawn;
        board[2][3] =pawn2;
        board[2][5] =pawn3;
        assertEquals(2, pawn.getPossibleMoves().size());
    }

    @Test
    public void testIsPossibleMoveWhiteIfTheOpponentIsAnotherColor() {
        Piece[][] board = new Piece[8][8];
        Pawn pawn = new Pawn(Color.W, new Position(3,4), board);
        pawn.setDoesMoved(true);
        Pawn pawn2 = new Pawn(Color.B, new Position(2,3), board);
        board[3][4] = pawn;
        board[2][3] =pawn2;
        assertTrue(pawn.isPossibleMove(new Position(2,3)));
    }
    @Test
    public void testIsPossibleMoveBlackIfTheOpponentIsAnotherColor() {
        Piece[][] board = new Piece[8][8];
        Pawn pawn = new Pawn(Color.B, new Position(3,4), board);
        pawn.setDoesMoved(true);
        Pawn pawn2 = new Pawn(Color.W, new Position(4,3), board);
        board[3][4] = pawn;
        board[4][3] =pawn2;
        assertTrue(pawn.isPossibleMove(new Position(4,3)));
    }

    @Test
    public void testIsNotPossibleMoveBlackIfTheOpponentIsSameColor() {
        Piece[][] board = new Piece[8][8];
        Pawn pawn = new Pawn(Color.B, new Position(3,4), board);
        pawn.setDoesMoved(true);
        Pawn pawn2 = new Pawn(Color.B, new Position(4,3), board);
        board[3][4] = pawn;
        board[4][3] =pawn2;
        assertFalse(pawn.isPossibleMove(new Position(4,3)));
    }

    @Test
    public void testIsNotPossibleMoveWhiteIfTheOpponentIsSamerColor() {
        Piece[][] board = new Piece[8][8];
        Pawn pawn = new Pawn(Color.W, new Position(3,4), board);
        pawn.setDoesMoved(true);
        Pawn pawn2 = new Pawn(Color.W, new Position(2,3), board);
        board[3][4] = pawn;
        board[2][3] =pawn2;
        assertFalse(pawn.isPossibleMove(new Position(2,3)));
    }

    @Test
    public void testIsPossibleMoveNextPositionIfIsEmpty() {
        Piece[][] board = new Piece[8][8];
        Pawn pawn = new Pawn(Color.W, new Position(3,4), board);
        pawn.setDoesMoved(true);
        board[3][4] = pawn;
        assertTrue(pawn.isPossibleMove(new Position(2,4)));
    }
    @Test
    public void testIsPossibleMoveNextPositionIfNotOccupiedDifferentColor() {
        Piece[][] board = new Piece[8][8];
        Pawn pawn = new Pawn(Color.W, new Position(3,4), board);
        pawn.setDoesMoved(true);
        Pawn pawn2 = new Pawn(Color.B, new Position(2,4), board);
        board[3][4] = pawn;
        board[2][3] =pawn2;
        assertTrue(pawn.isPossibleMove(new Position(2,4)));
    }

    @Test
    public void testMovingThePieceChangesPositionIfIsPossible() {
        Piece[][] board = new Piece[8][8];
        Pawn pawn = new Pawn(Color.W, new Position(3,4), board);
        board[3][4] = pawn;
        pawn.move(new Position(2, 4));
        assertEquals(2, pawn.getPosition().getRow());
        assertEquals(4, pawn.getPosition().getColumn());
    }

    @Test
    public void testDoesNotMoveIfThePositionIsOcuppiedSameColor() {
        Piece[][] board = new Piece[8][8];
        Pawn pawn = new Pawn(Color.W, new Position(3,4), board);
        Pawn pawn3 = new Pawn(Color.W, new Position(2,4), board);
        board[3][4] = pawn;
        board[2][4] = pawn3;
        pawn.move(new Position(2, 4));
        assertEquals(3, pawn.getPosition().getRow());
        assertEquals(4, pawn.getPosition().getColumn());
    }

    @Test
    public void testMovesIfThePositionIsOcuppiedDifferentColor() {
        Piece[][] board = new Piece[8][8];
        Pawn pawn = new Pawn(Color.W, new Position(3,4), board);
        pawn.setDoesMoved(true);
        Pawn pawn3 = new Pawn(Color.B, new Position(2,4), board);
        board[3][4] = pawn;
        board[2][4] = pawn3;
        pawn.move(new Position(2, 4));
        assertEquals(3, pawn.getPosition().getRow());
        assertEquals(4, pawn.getPosition().getColumn());
    }

    @Test
    public void testNotMovesIfThePositionIsOcuppiedDifferentColorBlack() {
        Piece[][] board = new Piece[8][8];
        Pawn pawn = new Pawn(Color.B, new Position(3,4), board);
        pawn.setDoesMoved(true);
        Pawn pawn3 = new Pawn(Color.W, new Position(4,4), board);
        board[3][5] = pawn;
        board[4][4] = pawn3;
        pawn.move(new Position(4, 4));
        assertEquals(3, pawn.getPosition().getRow());
        assertEquals(4, pawn.getPosition().getColumn());
    }

    @Test
    public void testMovingAPieceItChangesStateDoesMovesToTrue() {
        Piece[][] board = new Piece[8][8];
        Pawn pawn = new Pawn(Color.B, new Position(1,2), board);
        board[1][2] = pawn;
        pawn.move(new Position(2, 2));
        assertTrue(pawn.getDoesMoved());
    }
    @Test
    public void testDestroyingAPieceItChangesStateDoesExistToFalse() {
        Piece[][] board = new Piece[8][8];
        Pawn pawn = new Pawn(Color.B, new Position(3,4), board);
        board[3][5] = pawn;
        pawn.destroy(pawn);
        assertFalse(pawn.doesExist());

    }
    @Test
    public void testDestroyingAPieceItLosePosition() {
        Piece[][] board = new Piece[8][8];
        Pawn pawn = new Pawn(Color.B, new Position(3,4), board);
        board[3][5] = pawn;
        pawn.destroy(pawn);
        assertEquals(null, pawn.getPosition());
    }
}
