import static org.junit.Assert.assertTrue;
import org.junit.Test;

import java.util.ArrayList;

public class CreatorPiecesTest {
    private ArrayList<Piece> wPieces = new ArrayList<Piece>();
    private ArrayList<Piece> bPieces = new ArrayList<Piece>();
    Piece[][] board;

    @Test
    public void testAddElementBishopToListOfWhitePieces() {
        CreatorPieces creatorPieces = new CreatorPieces(wPieces, bPieces, board);
        creatorPieces.createBishops();
        int sizeList = creatorPieces.getWhitePieces().size();
        assertTrue(sizeList == 2);
    }
    @Test
    public void testAddElementBishopToListOfBlackPieces() {
        CreatorPieces creatorPieces = new CreatorPieces(wPieces, bPieces, board);
        creatorPieces.createBishops();
        int sizeList = creatorPieces.getBlackPieces().size();
        assertTrue(sizeList == 2);
    }
    @Test
    public void testAddElementRookToListOfWhitePieces() {
        CreatorPieces creatorPieces = new CreatorPieces(wPieces, bPieces, board);
        creatorPieces.createRooks();
        int sizeList = creatorPieces.getWhitePieces().size();
        assertTrue(sizeList == 2);
    }
    @Test
    public void testAddElementRookToListOfBlackPieces() {
        CreatorPieces creatorPieces = new CreatorPieces(wPieces, bPieces, board);
        creatorPieces.createRooks();
        int sizeList = creatorPieces.getBlackPieces().size();
        assertTrue(sizeList == 2);
    }
    @Test
    public void testAddElementQueenToListOfWhitePieces() {
        CreatorPieces creatorPieces = new CreatorPieces(wPieces, bPieces, board);
        creatorPieces.createQueens();
        int sizeList = creatorPieces.getWhitePieces().size();
        assertTrue(sizeList == 1);
    }
    @Test
    public void testAddElementQueenToListOfBlackPieces() {
        CreatorPieces creatorPieces = new CreatorPieces(wPieces, bPieces, board);
        creatorPieces.createQueens();
        int sizeList = creatorPieces.getBlackPieces().size();
        assertTrue(sizeList == 1);
    }
    @Test
    public void testAddElementKingToListOfWhitePieces() {
        CreatorPieces creatorPieces = new CreatorPieces(wPieces, bPieces, board);
        creatorPieces.createKings();
        int sizeList = creatorPieces.getWhitePieces().size();
        assertTrue(sizeList == 1);
    }
    @Test
    public void testAddElementKingToListOfBlackPieces() {
        CreatorPieces creatorPieces = new CreatorPieces(wPieces, bPieces, board);
        creatorPieces.createKings();
        int sizeList = creatorPieces.getBlackPieces().size();
        assertTrue(sizeList == 1);
    }
    @Test
    public void testAddElementHorseToListOfWhitePieces() {
        CreatorPieces creatorPieces = new CreatorPieces(wPieces, bPieces, board);
        creatorPieces.createHorses();
        int sizeList = creatorPieces.getWhitePieces().size();
        assertTrue(sizeList == 2);
    }
    @Test
    public void testAddElementHorseToListOfBlackPieces() {
        CreatorPieces creatorPieces = new CreatorPieces(wPieces, bPieces, board);
        creatorPieces.createHorses();
        int sizeList = creatorPieces.getBlackPieces().size();
        assertTrue(sizeList == 2);
    }
    @Test
    public void testAddElementPawnToListOfWhitePieces() {
        CreatorPieces creatorPieces = new CreatorPieces(wPieces, bPieces, board);
        creatorPieces.createPawns();
        int sizeList = creatorPieces.getWhitePieces().size();
        assertTrue(sizeList == 8);
    }
    @Test
    public void testAddElementPawnToListOfBlackPieces() {
        CreatorPieces creatorPieces = new CreatorPieces(wPieces, bPieces, board);
        creatorPieces.createPawns();
        int sizeList = creatorPieces.getBlackPieces().size();
        assertTrue(sizeList == 8);
    }
}
