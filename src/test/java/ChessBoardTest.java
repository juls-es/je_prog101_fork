import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class ChessBoardTest {

    private final static int SIZE = 8;
    Piece[][] board = new Piece[SIZE][SIZE];
    ChessBoard chessboard = new ChessBoard();
    Position source = new Position(1, 0);
    String input = "S(7a)T(8a)";
    ChessBoard chessBoard = new ChessBoard();

    @Test
    public void testGetPieces() {
        assertNotNull(chessboard.getPieces());
    }
    @Test
    public void testFillPiecesPositionZeroIsARook() {
        chessboard.fillPieces();
        Piece[][] actual = chessboard.getBoard();
        assertTrue(actual[0][0] instanceof Rook);
    }
    @Test
    public void testFillPiecesPositionOneIsAHorse() {
        chessboard.fillPieces();
        Piece[][] actual = chessboard.getBoard();
        assertTrue(actual[0][1] instanceof Horse);
    }
    @Test
    public void testFillPiecesPositionZTwoIsABishop() {
        chessboard.fillPieces();
        Piece[][] actual = chessboard.getBoard();
        assertTrue(actual[0][2] instanceof Bishop);
    }
    @Test
    public void testFillPiecesPositionThreeIsAQueen() {
        chessboard.fillPieces();
        Piece[][] actual = chessboard.getBoard();
        assertTrue(actual[0][3] instanceof Queen);
    }
    @Test
    public void testFillPiecesPositionFourIsAKing() {
        chessboard.fillPieces();
        Piece[][] actual = chessboard.getBoard();
        assertTrue(actual[0][4] instanceof King);
    }
    @Test
    public void testFillPiecesPositionZeroIsAPawn() {
        chessboard.fillPieces();
        Piece[][] actual = chessboard.getBoard();
        assertTrue(actual[1][0] instanceof Pawn);
    }
    @Test
    public void fillBoardWithAPawn() {
        Pawn pawn = new Pawn(Color.W, new Position(2, 2), board);
        pawn.setSource(source);
        pawn.setDoesMoved(true);
        chessboard.fillBoard(pawn);
        board = chessboard.getBoard();
        assertTrue(board[2][2] instanceof Pawn);
    }
    @Test
    public void fillBoardWithARook() {
        Rook rook = new Rook(Color.W, new Position(2, 2), board);
        rook.setSource(source);
        rook.setDoesMoved(true);
        chessboard.fillBoard(rook);
        board = chessboard.getBoard();
        assertTrue(board[2][2] instanceof Rook);
    }
    @Test
    public void fillBoardWithABishop() {
        Bishop bishop = new Bishop(Color.W, new Position(2, 2), board);
        bishop.setSource(source);
        bishop.setDoesMoved(true);
        chessboard.fillBoard(bishop);
        board = chessboard.getBoard();
        assertTrue(board[2][2] instanceof Bishop);
    }
    @Test
    public void fillBoardWithAHorse() {
        Horse horse = new Horse(Color.W, new Position(2, 2), board);
        horse.setSource(source);
        horse.setDoesMoved(true);
        chessboard.fillBoard(horse);
        board = chessboard.getBoard();
        assertTrue(board[2][2] instanceof Horse);
    }
    @Test
    public void fillBoardWithAQueen() {
        Queen queen = new Queen(Color.W, new Position(2, 2), board);
        queen.setSource(source);
        queen.setDoesMoved(true);
        chessboard.fillBoard(queen);
        board = chessboard.getBoard();
        assertTrue(board[2][2] instanceof Queen);
    }
    @Test
    public void fillBoardWithAKing() {
        King king = new King(Color.W, new Position(2, 2), board);
        king.setSource(source);
        king.setDoesMoved(true);
        chessboard.fillBoard(king);
        board = chessboard.getBoard();
        assertTrue(board[2][2] instanceof King);
    }
    @Test
    public void testClearPieces() {
        chessboard.clear();
        assertTrue(chessboard.getPieces().isEmpty());
    }
    @Test
    public void testClearBoard() {
        chessboard.clear();
        board = chessboard.getBoard();
        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < 8; j++) {
                assertTrue(board[i][j] == null);
            }
        }
    }
    @Test
    public void testGetPieceRook() {
        chessboard.fillPieces();
        Piece piece = chessboard.getPiece(new Position(0, 0));
        assertTrue(piece instanceof Rook);
    }
    @Test
    public void testGetPieceHorse() {
        chessboard.fillPieces();
        Piece piece = chessboard.getPiece(new Position(0, 1));
        assertTrue(piece instanceof Horse);
    }
    @Test
    public void testGetPieceBishop() {
        chessboard.fillPieces();
        Piece piece = chessboard.getPiece(new Position(0, 2));
        assertTrue(piece instanceof Bishop);
    }
    @Test
    public void testGetPieceQueen() {
        chessboard.fillPieces();
        Piece piece = chessboard.getPiece(new Position(0, 3));
        assertTrue(piece instanceof Queen);
    }
    @Test
    public void testGetPieceKing() {
        chessboard.fillPieces();
        Piece piece = chessboard.getPiece(new Position(0, 4));
        assertTrue(piece instanceof King);
    }
    @Test
    public void testGetPiecePawn() {
        chessboard.fillPieces();
        Piece piece = chessboard.getPiece(new Position(1, 0));
        assertTrue(piece instanceof Pawn);
    }
    @Test
    public void testCanPromote() throws Exception {
        Pawn pawn = new Pawn(Color.W, new Position(1, 0), board);
        board[1][0] = pawn;
        String inputString = "S(6a)T(7a)";
        chessboard.setBoard(board);
        chessboard.canPromote(inputString);
        boolean promotable = true;
        boolean actual = (board[1][0] instanceof Pawn) && promotable;
        assertTrue(actual);
    }
    @Test
    public void tsetPromoteToRook() throws Exception {
        Pawn pawn = new Pawn(Color.W, new Position(1, 0), board);
        pawn.setSource(source);
        board[1][0] = pawn;
        chessboard.setBoard(board);
        chessboard.promoteTo(input, "R");
        assertTrue(board[0][0] instanceof Rook);
    }
    @Test
    public void tsetPromoteToHorse() throws Exception {
        Pawn pawn = new Pawn(Color.W, new Position(1, 0), board);
        pawn.setSource(source);
        board[1][0] = pawn;
        chessboard.setBoard(board);
        chessboard.promoteTo(input, "H");
        assertTrue(board[0][0] instanceof Horse);
    }
    @Test
    public void tsetPromoteToBishop() throws Exception {
        Pawn pawn = new Pawn(Color.W, new Position(1, 0), board);
        pawn.setSource(source);
        board[1][0] = pawn;
        chessboard.setBoard(board);
        chessboard.promoteTo(input, "B");
        assertTrue(board[0][0] instanceof Bishop);
    }
    @Test
    public void tsetPromoteToQueen() throws Exception {
        Pawn pawn = new Pawn(Color.W, new Position(1, 0), board);
        pawn.setSource(source);
        board[1][0] = pawn;
        chessboard.setBoard(board);
        chessboard.promoteTo(input, "Q");
        assertTrue(board[0][0] instanceof Queen);
    }
    @Test
    public void testMovePawn() throws Exception {
        String inputStringP = "S(7a)T(6a)";
        chessboard.move(inputStringP);
        assertTrue(chessboard.getBoard()[2][0] instanceof Pawn);
    }
    @Test
    public void testMoveRook() throws Exception {
        Rook rook = new Rook(Color.B, new Position(0, 0), board);
        board[0][0] = rook;
        String inputStringP = "S(8a)T(5a)";
        chessboard.setBoard(board);
        chessboard.move(inputStringP);
        assertTrue(chessboard.getBoard()[3][0] instanceof Rook);
    }
    @Test
    public void testMoveHorse() throws Exception {
        String inputStringP = "S(8b)T(6a)";
        chessboard.move(inputStringP);
        assertTrue(chessboard.getBoard()[2][0] instanceof Horse);
    }
    @Test
    public void testMoveBishop() throws Exception {
        Bishop bishop = new Bishop(Color.B, new Position(0, 2), board);
        board[0][2] = bishop;
        String inputStringP = "S(8c)T(6a)";
        chessboard.setBoard(board);
        chessboard.move(inputStringP);
        assertTrue(chessboard.getBoard()[2][0] instanceof Bishop);
    }
    @Test
    public void testMoveQueen() throws Exception {
        Queen queen = new Queen(Color.B, new Position(0, 3), board);
        board[0][3] = queen;
        String inputStringP = "S(8d)T(8h)";
        chessboard.setBoard(board);
        chessboard.move(inputStringP);
        assertTrue(chessboard.getBoard()[0][7] instanceof Queen);
    }
    @Test
    public void testMoveKing() throws Exception {
        King king = new King(Color.B, new Position(0, 3), board);
        board[0][3] = king;
        String inputStringP = "S(8d)T(8e)";
        chessboard.setBoard(board);
        chessboard.move(inputStringP);
        assertTrue(chessboard.getBoard()[0][4] instanceof King);
    }
    @Test
    public void testGetKing() {
        Piece piece = chessBoard.getKing();
        assertTrue(piece instanceof King);
    }
    @Test
    public void testShortCastKingPositionOfRook() {
        King king = new King(Color.W, new Position(7, 4), board);
        board[7][4] = king;
        Rook rook = new Rook(Color.W, new Position(7, 7), board);
        board[7][7] = rook;
        String type = "4";
        chessBoard.setBoard(board);
        chessBoard.castKing(type);
        assertTrue(board[7][5] instanceof Rook);
    }
    @Test
    public void testShortCastKingPositionofKing() {
        King king = new King(Color.W, new Position(7, 4), board);
        board[7][4] = king;
        Rook rook = new Rook(Color.W, new Position(7, 7), board);
        board[7][7] = rook;
        String type = "4";
        chessBoard.setBoard(board);
        chessBoard.castKing(type);
        assertTrue(board[7][6] instanceof King);
    }
    @Test
    public void testLongCastKingPositionOfRook() {
        King king = new King(Color.W, new Position(7, 4), board);
        board[7][4] = king;
        Rook rook = new Rook(Color.W, new Position(7, 0), board);
        board[7][0] = rook;
        String type = "5";
        chessBoard.setBoard(board);
        chessBoard.castKing(type);
        assertTrue(board[7][3] instanceof Rook);
    }
    @Test
    public void testLongCastKingPositionofKing() {
        King king = new King(Color.W, new Position(7, 4), board);
        board[7][4] = king;
        Rook rook = new Rook(Color.W, new Position(7, 0), board);
        board[7][0] = rook;
        String type = "5";
        chessBoard.setBoard(board);
        chessBoard.castKing(type);
        assertTrue(board[7][2] instanceof King);
    }
}