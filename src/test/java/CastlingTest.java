import org.junit.Test;

import static org.junit.Assert.*;
public class CastlingTest {

    @Test
    public void testShortCastlingWhenRookAndKingAreAloneNotMoved() {
        Piece[][] board = new Piece[8][8];
        King king = new King(Color.B, new Position(0, 4), board);
        Rook rook = new Rook(Color.B, new Position(0, 7), board);
        board[0][4] = king;
        board[0][7] = rook;
        king.shortCastling();
        assertEquals(0, king.getPosition().getRow());
        assertEquals(6, king.getPosition().getColumn());
        assertEquals(0, rook.getPosition().getRow());
        assertEquals(5, rook.getPosition().getColumn());
    }

    @Test
    public void testShortCastlingWhenRookAndKingAreAloneMoved() {
        Piece[][] board = new Piece[8][8];
        King king = new King(Color.B, new Position(0, 4), board);
        king.setDoesMoved(true);
        Rook rook = new Rook(Color.B, new Position(0, 7), board);
        board[0][4] = king;
        board[0][7] = rook;
        king.shortCastling();
        assertEquals(0, king.getPosition().getRow());
        assertEquals(4, king.getPosition().getColumn());
        assertEquals(0, rook.getPosition().getRow());
        assertEquals(7, rook.getPosition().getColumn());
    }

    @Test
    public void testLargeCastlingWhenRookAndKingAreAloneNotMoved() {
        Piece[][] board = new Piece[8][8];
        King king = new King(Color.W, new Position(7, 4), board);
        Rook rook = new Rook(Color.W, new Position(7, 0), board);
        board[7][4] = king;
        board[7][0] = rook;
        king.largeCastling();
        assertEquals(7, king.getPosition().getRow());
        assertEquals(2, king.getPosition().getColumn());
        assertEquals(7, rook.getPosition().getRow());
        assertEquals(3, rook.getPosition().getColumn());
    }

    @Test
    public void testLargeCastlingWhenRookAndKingAreAloneMoved() {
        Piece[][] board = new Piece[8][8];
        King king = new King(Color.W, new Position(7, 4), board);
        Rook rook = new Rook(Color.W, new Position(7, 0), board);
        rook.setDoesMoved(true);
        board[7][4] = king;
        board[7][0] = rook;
        king.largeCastling();
        assertEquals(7, king.getPosition().getRow());
        assertEquals(4, king.getPosition().getColumn());
        assertEquals(7, rook.getPosition().getRow());
        assertEquals(0, rook.getPosition().getColumn());
    }
    @Test
    public void tesNotLargeCastlingWhenKingIsInCheck() {
        Piece[][] board = new Piece[8][8];
        King king = new King(Color.W, new Position(7, 4), board);
        Rook rook = new Rook(Color.W, new Position(7, 0), board);
        Pawn pawn = new Pawn(Color.B, new Position(6, 3), board);
        board[6][3] = pawn;
        board[7][4] = king;
        board[7][0] = rook;
        king.largeCastling();
        assertEquals(7, king.getPosition().getRow());
        assertEquals(4, king.getPosition().getColumn());
        assertEquals(7, rook.getPosition().getRow());
        assertEquals(0, rook.getPosition().getColumn());
    }

    @Test
    public void tesNotShortCastlingWhenKingIsInCheck() {
        Piece[][] board = new Piece[8][8];
        King king = new King(Color.W, new Position(7, 4), board);
        Rook rook = new Rook(Color.W, new Position(7, 7), board);
        Pawn pawn = new Pawn(Color.B, new Position(6, 5), board);
        board[6][5] = pawn;
        board[7][4] = king;
        board[7][7] = rook;
        king.largeCastling();
        assertEquals(7, king.getPosition().getRow());
        assertEquals(4, king.getPosition().getColumn());
        assertEquals(7, rook.getPosition().getRow());
        assertEquals(7, rook.getPosition().getColumn());
    }
}
