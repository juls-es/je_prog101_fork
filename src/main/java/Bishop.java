import java.util.ArrayList;
import java.util.List;

public class Bishop extends Piece {
    private List<Position> positions;
    private static final int SIZE_BOARD = 8;

    public Bishop(final Color color, final Position position, final Piece[][] board) {
        super(color, position, Symbol.B, board);
        positions = new ArrayList<Position>();
    }

    /**
     * Destroys a piece
     *
     * @param
     */
    public void destroyPiece(final Piece piece) {
        piece.die();
        piece.setPosition(null);
    }

    /**
     * Destroys a piece
     */
    public void destroy() {

    }

    /**
     * Destroys a piece
     */
    @Override
    public void destroy(final Piece piece) {

    }

    /**
     * Changes the position
     *
     * @Override
     */
    public void move(final Position position) {
        if (isPossibleMove(position)) {
            if (thereIsPieceEnemy(position)) {
                destroyPiece(getPiece(position));
                super.setPosition(position);
            } else {
                super.setPosition(position);
            }
        }
    }

    /**
     * Returns true if is possible move to position
     *
     * @return boolean is posible move to a position
     */
    @Override
    public boolean isPossibleMove(final Position position) {
        if (thereIsPieceFriend(position)) {
            return false;
        } else {
            getPossibleMoves();
            for (Position pos : positions) {
                if (pos.getColumn() == position.getColumn() && pos.getRow() == position.getRow()) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Stores in a list the possible movements down right
     */
    public void possibleMovesDownRight() {
        int column = this.getPosition().getColumn() + 1;
        for (int i = this.getPosition().getRow() + 1; i < SIZE_BOARD; i++) {
            if (column < SIZE_BOARD) {
                Position newPosition = new Position(i, column);
                if (!thereIsPiece(newPosition) || thereIsPieceEnemy(newPosition)) {
                    positions.add(newPosition);
                    column++;
                } else {
                    i = SIZE_BOARD;
                }
            } else {
                i = SIZE_BOARD;
            }
        }
    }

    /**
     * Stores in a list the possible movements down left
     */
    public void posibleMovesDownLeft() {
        int column = this.getPosition().getColumn() - 1;
        for (int i = this.getPosition().getRow() + 1; i < SIZE_BOARD; i++) {
            if (column >= 0) {
                Position newPosition = new Position(i, column);
                if (!thereIsPiece(newPosition) || thereIsPieceEnemy(newPosition)) {
                    positions.add(newPosition);
                    column--;
                } else {
                    i = SIZE_BOARD;
                }
            } else {
                i = SIZE_BOARD;
            }
        }
    }

    /**
     * Stores in a list the possible movements up right
     */
    public void possibleMovesUpRight() {
        int column = this.getPosition().getColumn() + 1;
        for (int i = this.getPosition().getRow() - 1; i >= 0; i--) {
            if (column < SIZE_BOARD) {
                Position newPosition = new Position(i, column);
                if (!thereIsPiece(newPosition) || thereIsPieceEnemy(newPosition)) {
                    positions.add(newPosition);
                    column++;
                } else {
                    i = -1;
                }
            } else {
                i = -1;
            }
        }
    }

    /**
     * Stores in a list the possible movements up left
     */
    public void possibleMovesUpLeft() {
        int column = this.getPosition().getColumn() - 1;
        for (int i = this.getPosition().getRow() - 1; i >= 0; i--) {
            if (column >= 0) {
                Position newPosition = new Position(i, column);
                if (!thereIsPiece(newPosition) || thereIsPieceEnemy(newPosition)) {
                    positions.add(newPosition);
                    column--;
                } else {
                    i = -1;
                }
            } else {
                i = -1;
            }
        }
    }

    /**
     * Returns a list with all of possible movements
     *
     * @return a List with possible movements
     */
    @Override
    public List<Position> getPossibleMoves() {
        positions.clear();
        posibleMovesDownLeft();
        possibleMovesDownRight();
        possibleMovesUpLeft();
        possibleMovesUpRight();
        return positions;
    }

    /**
     * Returns if there are a piece in the position
     *
     * @param position
     * @return boolean
     */
    public boolean thereIsPiece(final Position position) {
        return (getBoard()[position.getRow()][position.getColumn()] != null);
    }

    /**
     * Returns if there are a piece friend in the position
     *
     * @param position
     * @return boolean
     */
    public boolean thereIsPieceFriend(final Position position) {
        return (getBoard()[position.getRow()][position.getColumn()] != null
                && getBoard()[position.getRow()][position.getColumn()].getColor() == getColor());
    }

    /**
     * Returns if there are a piece in the position
     *
     * @param position
     * @return boolean
     */
    public boolean thereIsPieceEnemy(final Position position) {
        return (getBoard()[position.getRow()][position.getColumn()] != null
                && getBoard()[position.getRow()][position.getColumn()].getColor() != getColor());
    }

    /**
     * Returns a Piece in the position
     *
     * @return Piece
     * @param position
     */
    public Piece getPiece(final Position position) {
        if (getBoard()[position.getRow()][position.getColumn()] != null) {
            return getBoard()[position.getRow()][position.getColumn()];
        } else {
            return null;
        }
    }
}
