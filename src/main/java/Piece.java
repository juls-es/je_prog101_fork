public abstract class Piece implements Movable {
    private Color color;
    private Symbol symbol;
    private Position position;
    private boolean doesExist;
    private boolean doesMoved;
    private Position source;
    private Piece[][] board;

    public Piece(final Color color, final Position position, final Symbol symbol, final boolean doesExist) {
        this.color = color;
        this.position = position;
        this.symbol = symbol;
        this.doesExist = doesExist;
        this.doesMoved = false;
    }

    public Piece(final Color color, final Position position, final Symbol symbol, final Piece[][] board) {
        this.color = color;
        this.position = position;
        this.symbol = symbol;
        this.doesExist = true;
        doesMoved = false;
        this.board = board;
    }

    /**
     * To destroy a piece in the board
     * @param piece - the reference piece to destroy
     */
    public abstract void destroy(Piece piece);

    /**
     * Set the existence of this object to false
     */
    public void die() {
        doesExist = false;
    }

    /**
     * @return position - the reference position of this object
     */
    public Position getPosition() {
        return position;
    }

    /**
     * Set the position of this object
     * @param position - the reference position to set to this object
     */
    public void setPosition(final Position position) {
        this.position = position;
    }

    /**
     * @return color - the reference color of this object
     */
    public Color getColor() {
        return color;
    }

    /**
     * @return symbol - the reference symbol of this object
    */
    public Symbol getSymbol() {
        return symbol;
    }

    /**
     * Set the color of this object
     * @param color - the reference Color to set to this object
    */
    public void setColor(final Color color) {
        this.color = color;
    }

    /**
     * Set the symbol of this object
     * @param symbol - the reference Symbol to set to this object
    */
    public void setSymbol(final Symbol symbol) {
        this.symbol = symbol;
    }


    /**
     * Set the existence of this object
     * @param doesExist - the boolean reference to set the existence of this object
    */
    public void setDoesExist(final boolean doesExist) {
        this.doesExist = doesExist;
    }
    /**
     * @return true if this object was moved; false otherwise
    */
    public boolean getDoesMoved() {
        return doesMoved;
    }
    /**
     * @param doesMoved - the boolean reference indicating that this object was moved
    */
    public void setDoesMoved(final boolean doesMoved) {
        this.doesMoved = doesMoved;
    }

    /**
     * @return true if this object exists; false otherwise
     */
    public boolean doesExist() {
        return doesExist;
    }

    /**
     * @return source position of this object
    */
    public Position getSource() {
        return source;
    }

    /**
     * Set the source position of this object
     * @param position - the position to set as a source of this object
    */
    public void setSource(final Position position) {
        this.source = position;
    }

    /**
     * Set the board of this object
     * @param board - the chess board with pieces to set to this object
    */
    public void setBoard(final Piece[][] board) {
        this.board = board;
    }

    /**
     * @return board of this object
    */
    public Piece[][] getBoard() {
        return board;
    }
}
