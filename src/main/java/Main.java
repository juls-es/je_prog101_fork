import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Main {
    private ChessBoard chessBoard;
    private String response = "";

    public Main() {
        chessBoard = new ChessBoard();
    }

    /**
     * start the game console
     */
    public void start() {
        String input = "";
        String movementToPromotion = "";
        BufferedReader inputReader = new BufferedReader(new InputStreamReader(System.in));
        while (true) {
            try {
                this.printMenu();
                response = inputReader.readLine();
                if (input.equalsIgnoreCase("0")) {
                    return;
                }
                switch (response) {
                    case "1":
                        System.out.println("Enter your move (format: S(2e)T(3e))");
                        input = inputReader.readLine();
                        if (chessBoard.isValidTurn(input)) {
                            if (chessBoard.canPromote(input)) {
                                movementToPromotion = input;
                                System.out.println("Insert a Symbol to promote: H, B, R, Q");
                                input = inputReader.readLine();
                                chessBoard.promoteTo(movementToPromotion, input);
                                } else {
                                    chessBoard.move(input);
                            }
                            if (chessBoard.isInCheck()) {
                                System.out.println("The king is in check");
                            }
                            if (chessBoard.isInCheckmate()) {
                                System.out.println("End game!!! The winner is: " + chessBoard.getWinner());
                                return;
                            }
                        } else {
                            System.out.println("It's not your turn");
                        }
                        break;
                    case "2":
                        System.out.println("Enter a piece (format: P(2e))");
                        input = inputReader.readLine();
                        if (chessBoard.isValidTurn(input)) {
                            chessBoard.displayPossibleMoves(input);
                        } else {
                            System.out.println("It's not your turn");
                        }
                        break;
                    case "3":
                        System.out.println("The game restarted Successfully");
                        this.restartGame();
                    case "4":
                        chessBoard.castKing(response);
                        break;
                    case "5":
                        chessBoard.castKing(response);
                        break;
                    case "0":
                        return;
                    default:
                        break;
                }

            } catch (Exception e) {
                System.out.println("Invalid value inserted");
            }
        }
    }

    /**
     * Prints a menu
     */
    public void printMenu() {
        String menu = "1.- Move a piece\n2.- Get possibles movements\n3.- Restart the game\n"
                    + "4.- Do a short castling\n5.- Do a long castling\n0.- Exit\n";
        System.out.println(menu);
        if (chessBoard.getTurn()) {
            System.out.println("White>");
        } else {
            System.out.println("Black>");
        }
    }

    /**
     * Restarts the game
     */
    public void restartGame() {
        chessBoard.clear();
        chessBoard.fillPieces();
    }
    public static void main(final String[] args) {
        System.out.println("----------------------------------------");
        System.out.println("        WELLCOME TO CHESS GAME");
        System.out.println("----------------------------------------");
        Main main = new Main();
        main.start();
    }
}
