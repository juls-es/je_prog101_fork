import java.util.ArrayList;
import java.util.List;

public class Queen extends Piece {
    public static final int SIZE_BOARD = 8;
    private List<Position> possiblesMovements;

    public Queen(final Color color, final Position position, final Piece[][] board) {
        super(color, position, Symbol.Q, board);
        possiblesMovements = new ArrayList<Position>();
    }

    /**
     * Destroys a piece
     *
     * @param piece
     * @param board
     */
    public void destroy(final Piece piece, final Piece[][] board) {
        board[piece.getPosition().getRow()][piece.getPosition().getColumn()] = null;
    }

    /**
     * Move a piece
     *
     * @param position
     */
    public void move(final Position position) {
        if (isPossibleMove(position)) {
            if (doesExistPiece(position)) {
                destroy(getPiece(position));
            }
            setPosition(position);
            setDoesMoved(true);
        }
    }

    /**
     * Returns true if is possible move to position
     * @param targetPosition
     */
    @Override
    public boolean isPossibleMove(final Position targetPosition) {
        for (Position pos : getPossibleMoves()) {
            if (pos.getColumn() == targetPosition.getColumn() && pos.getRow() == targetPosition.getRow()) {
                return true;
            }
        }
        return false;
    }

    /**
     * Get the list of possibles movements
     *
     * @return possibles movements
     */
    public List<Position> getPossiblesMovements() {
        return possiblesMovements;
    }

    /**
     * Return true if both pieces are the same color
     *@return boolean
     **/
    public boolean isSameColor(final Piece piece) {
        if (getColor() == piece.getColor()) {
            return true;
        }
        return false;
    }

    /**
     * Add to list of possibles movements to right
     **/
    public void getMovesRight() {
        int row = this.getPosition().getRow();
        int column = this.getPosition().getColumn();

        for (int i = column + 1; i < SIZE_BOARD; i++) {
            if (!doesExistPiece(new Position(row, i))) {
                //System.out.println(!doesExistPiece(new Position(row, i)));
                possiblesMovements.add(new Position(row, i));
            } else {
                if (!isSameColor(getPiece(new Position(row, i)))) {
                    possiblesMovements.add(new Position(row, i));
                }
                i = SIZE_BOARD;
            }
        }
    }

    /**
     * Add to list of possibles movements to left
     **/
    public void getMovesLeft() {
        int row = getPosition().getRow();
        int column = getPosition().getColumn();

        for (int i = column - 1; i > -1; i--) {
            if (!doesExistPiece(new Position(row, i))) {
                possiblesMovements.add(new Position(row, i));
            } else {
                if (!isSameColor(getPiece(new Position(row, i)))) {
                    possiblesMovements.add(new Position(row, i));
                }
                i = -1;
            }
        }
    }

    /**
     * Add to list of possibles movements to up
     **/
    public void getMovesUp() {
        int row = getPosition().getRow();
        int column = getPosition().getColumn();

        for (int i = row - 1; i > -1; i--) {
            if (!doesExistPiece(new Position(i, column))) {
                possiblesMovements.add(new Position(i, column));
            } else {
                if (!isSameColor(getPiece(new Position(i, column)))) {
                    possiblesMovements.add(new Position(i, column));
                }
                i = -1;
            }
        }
    }

    /**
     * Add to list of possibles movements to down
     **/
    public void getMovesDown() {
        int row = getPosition().getRow();
        int column = getPosition().getColumn();

        for (int i = row + 1; i < SIZE_BOARD; i++) {
            if (!doesExistPiece(new Position(i, column))) {
                possiblesMovements.add(new Position(i, column));
            } else {
                if (!isSameColor(getPiece(new Position(i, column)))) {
                    possiblesMovements.add(new Position(i, column));
                }
                i = SIZE_BOARD;
            }
        }
    }

    /**
     * Add to list of possibles movements top right
     **/
    public void getMovesTopRight() {
        int row = getPosition().getRow();
        int column = getPosition().getColumn();

        for (int i = 1; i < SIZE_BOARD; i++) {
            if ((row - i) > -1 && (column + i) <  SIZE_BOARD) {
                if (!doesExistPiece(new Position(row - i, column + i))) {
                    possiblesMovements.add(new Position(row - i, column + i));
                } else {
                    if (!isSameColor(getPiece(new Position(row - i, column + i)))) {
                        possiblesMovements.add(new Position(row - i, column + i));
                    }
                    i = SIZE_BOARD;
                }
            } else {
                i = SIZE_BOARD;
            }
        }
    }

    /**
     * Add to list of possibles movements top left
     **/
    public void getMovesTopLeft() {
        int row = getPosition().getRow();
        int column = getPosition().getColumn();

        for (int i = 1; i < SIZE_BOARD; i++) {
            if ((row - i) > -1 && (column - i) > -1) {
                if (!doesExistPiece(new Position(row - i, column - i))) {
                    possiblesMovements.add(new Position(row - i, column - i));
                } else {
                    if (!isSameColor(getPiece(new Position(row - i, column - i)))) {
                        possiblesMovements.add(new Position(row - i, column - i));
                    }
                    i = SIZE_BOARD;
                }
            } else {
                i = SIZE_BOARD;
            }
        }
    }

    /**
     * Add to list of possibles movements bottom left
     **/
    public void getMovesBottomLeft() {
        int row = getPosition().getRow();
        int column = getPosition().getColumn();

        for (int i = 1; i < SIZE_BOARD; i++) {
            if ((row + i) < SIZE_BOARD && (column - i) > -1) {
                if (!doesExistPiece(new Position(row + i, column - i))) {
                    possiblesMovements.add(new Position(row + i, column - i));
                } else {
                    if (!isSameColor(getPiece(new Position(row + i, column - i)))) {
                        possiblesMovements.add(new Position(row + i, column - i));
                    }
                    i = SIZE_BOARD;
                }
            } else {
                i = SIZE_BOARD;
            }
        }
    }

    /**
     * Add to list of possibles movements bottom right
     **/
    public void getMovesBottomRight() {
        int row = getPosition().getRow();
        int column = getPosition().getColumn();

        for (int i = 1; i < SIZE_BOARD; i++) {
            if ((row + i) < SIZE_BOARD && (column + i) < SIZE_BOARD) {
                if (!doesExistPiece(new Position(row + i, column + i))) {
                    possiblesMovements.add(new Position(row + i, column + i));
                } else {
                    if (!isSameColor(getPiece(new Position(row + i, column + i)))) {
                        possiblesMovements.add(new Position(row + i, column + i));
                    }
                    i = SIZE_BOARD;
                }
            } else {
                i = SIZE_BOARD;
            }
        }
    }

    /**
     * Return the list of possibles movements of piece
     * return
     **/
    @Override
    public List<Position> getPossibleMoves() {
        possiblesMovements.clear();
        getMovesRight();
        getMovesLeft();
        getMovesUp();
        getMovesDown();
        getMovesTopRight();
        getMovesTopLeft();
        getMovesBottomLeft();
        getMovesBottomRight();

        return possiblesMovements;
    }

    /**
     * Returns if there are a piece in the position
     * @param position
     * @return boolean
     */
    public boolean doesExistPiece(final Position position) {
        return (getBoard()[position.getRow()][position.getColumn()] != null);
    }

    /**
     * Returns a Piece in the position
     * @return Piece
     * @param position
     */
    public Piece getPiece(final Position position) {
        if (getBoard()[position.getRow()][position.getColumn()] != null) {
            return getBoard()[position.getRow()][position.getColumn()];
        } else {
            return null;
        }
    }
    /**
     * Destroys a piece
     *
     * @param piece
     */
    @Override
    public void destroy(final Piece piece) {
        piece.die();
        piece.setPosition(null);
    }
}
