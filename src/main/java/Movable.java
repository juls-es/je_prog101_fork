import java.util.List;

public interface Movable {
    void move(Position position);
    boolean isPossibleMove(Position position);
    List<Position> getPossibleMoves();
}
