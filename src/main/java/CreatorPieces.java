import java.util.List;

public class CreatorPieces {
    public static final int CANT_PAWN = 8;
    public static final int CANT_BISHOP = 2;
    public static final int POS_INI_PAWN_B = 1;
    public static final int POS_INI_PAWN_W = 6;
    public static final int ROW_BISHOP_W = 7;
    public static final int ROW_BISHOP_B = 0;
    public static final int COLUMN_BISHOP_FIRST = 2;
    public static final int COLUMN_BISHOP_LAST = 5;
    public static final int POS_INI_KING_BC = 4;
    public static final int POS_INI_KING_WR = 7;
    public static final int CANT_HORSE = 4;
    public static final int POS_INI_ROW_HORSE_B = 0;
    public static final int POS_INI_ROW_HORSE_W = 7;
    public static  final int POS_INI_QUEEN_B = 0;
    public static  final int POS_INI_QUEEN_W = 7;
    public static  final int POS_INI_QUEEN = 3;
    public static final int POS_ROOK_B = 0;
    public static final int POS_ROOK_W = 7;
    private List<Piece> wPieces;
    private List<Piece> bPieces;
    private Piece[][] board;

    public CreatorPieces(final List<Piece> wPieces, final List<Piece> bPieces, final Piece[][] board) {
        this.wPieces = wPieces;
        this.bPieces = bPieces;
        this.board = board;
    }

    /**
     * Creator of pieces of Chess
     */
    public void createPieces() {
        createPawns();
        createBishops();
        createKings();
        createHorses();
        createQueens();
        createRooks();
    }

    /** @Return pawns ***/
    // i change the cant_pawn
    public void createPawns() {
        for (int i = CANT_PAWN - 1; i >= 0; i--) {
            bPieces.add(new Pawn(Color.B, new Position(POS_INI_PAWN_B, i), board));
        }
        for (int i = CANT_PAWN - 1; i >= 0; i--) {
            wPieces.add(new Pawn(Color.W, new Position(POS_INI_PAWN_W, i), board));
        }
    }

    /** */
    public void createKings() {
        bPieces.add(new King(Color.B, new Position(0, POS_INI_KING_BC), board));
        wPieces.add(new King(Color.W, new Position(POS_INI_KING_WR, POS_INI_KING_BC), board));
    }

    /**
     * Create the bishops
     */
    public void createBishops() {
        bPieces.add(new Bishop(Color.B, new Position(ROW_BISHOP_B, COLUMN_BISHOP_FIRST), board));
        bPieces.add(new Bishop(Color.B, new Position(ROW_BISHOP_B, COLUMN_BISHOP_LAST), board));
        wPieces.add(new Bishop(Color.W, new Position(ROW_BISHOP_W, COLUMN_BISHOP_FIRST), board));
        wPieces.add(new Bishop(Color.W, new Position(ROW_BISHOP_W, COLUMN_BISHOP_LAST), board));
    }

    /**
     * Create Horses in initial positions of Chess
     */
    public void createHorses() {
        final int colValue = 7;
        final int shiftValue = 5;
            for (int j = 1; j < colValue; j = j + shiftValue) {
                bPieces.add(new Horse(Color.B, new Position(POS_INI_ROW_HORSE_B, j), board));
            }
            for (int j = 1; j < colValue; j = j + shiftValue) {
                wPieces.add(new Horse(Color.W, new Position(POS_INI_ROW_HORSE_W, j), board));
            }
    }
    /**
     * Create the Rooks
     */
    public void createRooks() {
        bPieces.add(new Rook(Color.B, new Position(POS_ROOK_B, POS_ROOK_B), board));
        bPieces.add(new Rook(Color.B, new Position(POS_ROOK_B, POS_ROOK_W), board));
        wPieces.add(new Rook(Color.W, new Position(POS_ROOK_W, POS_ROOK_B), board));
        wPieces.add(new Rook(Color.W, new Position(POS_ROOK_W, POS_ROOK_W), board));
    }

    /**
     * Create the Queens
     */
    public void createQueens() {
        bPieces.add(new Queen(Color.B, new Position(POS_INI_QUEEN_B,  POS_INI_QUEEN), board));
        wPieces.add(new Queen(Color.W, new Position(POS_INI_QUEEN_W, POS_INI_QUEEN), board));
    }

    /**
     * Clear references to the lists of white pieces and black pieces of this object
     */
    public void clearPieces() {
        wPieces.clear();
        bPieces.clear();
    }

    /**
     * Get black pieces of this object
     * @return List<Piece> - reference to the list of black pieces of this object
     */
    public List<Piece> getBlackPieces() {
        return bPieces;
    }

    /**
     * Get white pieces of this object
     * @return List<Piece> - reference to the list of white pieces of this object
     */
    public List<Piece> getWhitePieces() {
        return wPieces;
    }
}
