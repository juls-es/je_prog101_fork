import java.util.ArrayList;
import java.util.List;

public class Rook extends Piece {
    public static final int POS_INITIAL_W = 6;
    public static final int POS_INITIAL_B = 1;
    public static final int SIZE_BOARD = 8;
    private List<Position> possiblesMovements;

    public Rook(final Color color, final Position position, final Piece[][] board) {
        super(color, position, Symbol.R, board);
        possiblesMovements = new ArrayList<Position>();
    }

    /**
     * Destroys a piece
     *
     * @param
     */
    @Override
    public void destroy(final Piece piece) {
        piece.die();
        piece.setPosition(null);
    }

    /**
     * Destroys a piece
     */
    public void destroy() {

    }

    /**
     * Move a piece
     *
     * @param position
     */
    public void move(final Position position) {
        if (isPossibleMove(position)) {
            if (doesExistPiece(position)) {
                destroy(getPiece(position));
            }
            setPosition(position);
            setDoesMoved(true);
        }
    }

    /**
     * Returns true if is possible move to position
     * @param targetPosition
     */
    @Override
    public boolean isPossibleMove(final Position targetPosition) {
        for (Position pos : getPossibleMoves()) {
            if (pos.getColumn() == targetPosition.getColumn() && pos.getRow() == targetPosition.getRow()) {
                return true;
            }
        }
        return false;
    }

    /**
     * Get the list of possibles movements
     *
     * @return possibles movements
     */
    public List<Position> getPossiblesMovements() {
        return possiblesMovements;
    }

    /**
     * Return true if both pieces are the same color
     *@return boolean
     **/
    public boolean isSameColor(final Piece piece) {
        if (getColor() == piece.getColor()) {
            return true;
        }
        return false;
    }

    /**
     * Add to list of possibles movements to down
     **/
    public void getMovesDown() {
        int row = getPosition().getRow();
        int column = getPosition().getColumn();

        for (int i = row + 1; i < SIZE_BOARD; i++) {
            if (!doesExistPiece(new Position(i, column))) {
                possiblesMovements.add(new Position(i, column));
            } else {
                if (!isSameColor(getPiece(new Position(i, column)))) {
                    possiblesMovements.add(new Position(i, column));
                }
                i = SIZE_BOARD;
            }
        }
    }

    /**
     * Add to list of possibles movements to right
     **/
    public void getMovesRight() {
        int row = this.getPosition().getRow();
        int column = this.getPosition().getColumn();

        for (int i = column + 1; i < SIZE_BOARD; i++) {
            if (!doesExistPiece(new Position(row, i))) {
                possiblesMovements.add(new Position(row, i));
            } else {
                if (!isSameColor(getPiece(new Position(row, i)))) {
                    possiblesMovements.add(new Position(row, i));
                }
                i = SIZE_BOARD;
            }
        }
    }

    /**
     * Add to list of possibles movements to left
     **/
    public void getMovesLeft() {
        int row = getPosition().getRow();
        int column = getPosition().getColumn();

        for (int i = column - 1; i > -1; i--) {
            if (!doesExistPiece(new Position(row, i))) {
                possiblesMovements.add(new Position(row, i));
            } else {
                if (!isSameColor(getPiece(new Position(row, i)))) {
                    possiblesMovements.add(new Position(row, i));
                }
                i = -1;
            }
        }
    }

    /**
     * Add to list of possibles movements to up
     **/
    public void getMovesUp() {
        int row = getPosition().getRow();
        int column = getPosition().getColumn();

        for (int i = row - 1; i > -1; i--) {
            if (!doesExistPiece(new Position(i, column))) {
                possiblesMovements.add(new Position(i, column));
            } else {
                if (!isSameColor(getPiece(new Position(i, column)))) {
                    possiblesMovements.add(new Position(i, column));
                }
                i = -1;
            }
        }
    }

    /**
     * Return the list of possibles movements of piece
     * @return possiblesMovements
     **/
    @Override
    public List<Position> getPossibleMoves() {
        possiblesMovements.clear();
        getMovesRight();
        getMovesLeft();
        getMovesUp();
        getMovesDown();
        return possiblesMovements;
    }

    /**
     * Returns if there are a piece in the position
     * @param position
     * @return boolean
     */
    public boolean doesExistPiece(final Position position) {
        return (getBoard()[position.getRow()][position.getColumn()] != null);
    }

    /**
     * Returns a Piece in the position
     * @return Piece
     * @param position
     */
    public Piece getPiece(final Position position) {
        if (getBoard()[position.getRow()][position.getColumn()] != null) {
            return getBoard()[position.getRow()][position.getColumn()];
        } else {
            return null;
        }
    }
}
