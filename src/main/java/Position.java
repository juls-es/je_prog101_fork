public class Position {
    private int row;
    private int column;

    public Position(final int x, final int y) {
        row = x;
        column = y;
    }

    /**@Return row y***/
    public int getRow() {
        return row;
    }

    /**@param***/
    public void setRow(final int x) throws Exception {
        if (x >= 0 && x < ChessBoard.SIZE_BOARD) {
            row = x;
        } else {
            throw new Exception("Invalid row sent: " + x);
        }
    }

    /**@Return column***/
    public int getColumn() {
        return column;
    }

    /**@param***/
    public void setColumn(final int y) {
        column = y;
    }

    /*@Override
    public boolean equals(Object objectPosition) {
        Position position = (Position)objectPosition;
        return position.getColumn() == this.getColumn() && position.getRow() == this.getRow();
    }*/

    /**
     * @param position
     * @return
     */
    public boolean equals(final Position position) {
        return position.getColumn() == this.getColumn() && position.getRow() == this.getRow();
    }
}
