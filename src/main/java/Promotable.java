public interface Promotable {
    void promotion(Position position, String symbol);
    boolean canPromote(Position position);
}
