import java.util.ArrayList;
import java.util.List;

public class Parser {
    private String input;
    private List<int[]> positionToMove;
    private int sourceRow;
    private int sourceColumn;
    private int targetRow;
    private int targetColumn;
    private Position position;
    private static final int INDEX_ONE = 1;
    private static final int INDEX_TWO = 2;
    private static final int INDEX_THREE = 3;
    private static final int INDEX_FOUR = 4;
    private static final int INDEX_FIVE = 5;
    private static final int INDEX_SIX = 6;
    private static final int INDEX_SEVEN = 7;
    private static final int INDEX_EIGHT = 8;
    private static final int SIZE = 2;

    public Parser(final String inputString) {
        this.input = inputString;
    }
    public Parser(final Position position) {
        this.position = position;
    }

     /**
     * Gets the number corresponding to the letter of the column of the chess board
     * @param number - the reference char with which compare to select a number of column of the board
     * @return int - the number corresponding to the column in the board of this object
     */
    public static int getNumber(final char number) {
        switch (number) {
            case 'a':
                return 0;
            case 'b':
                return INDEX_ONE;
            case 'c':
                return INDEX_TWO;
            case 'd':
                return INDEX_THREE;
            case 'e':
                return INDEX_FOUR;
            case 'f':
                return INDEX_FIVE;
            case 'g':
                return INDEX_SIX;
            case 'h':
                return INDEX_SEVEN;
            default:
                return -1;
        }
    }

    /**
     * Gets the letter corresponding to the number of the column of the chess board
     * @param number - the number corresponding to the column in the board of this object
     * @return letter - the reference string corresponding to the column in the board of this object
     */
    public String getLetter(final int number) {
        switch (number) {
            case 0:
                return "a";
            case INDEX_ONE:
                return "b";
            case INDEX_TWO:
                return "c";
            case INDEX_THREE:
                return "d";
            case INDEX_FOUR:
                return "e";
            case INDEX_FIVE:
                return "f";
            case INDEX_SIX:
                return "g";
            case INDEX_SEVEN:
                return "h";
            default:
                return "";
        }
    }

    /**
     * Returns the index number of the matrix corresponding to the String Entered
     * @param substring - the number to convert to its corresponding index of the matrix
     * @return int - number equivalent to the index of the matrix
     */
    public int getEquivalent(final String indexString) {
        int integerRead = Integer.parseInt(indexString);
        switch (integerRead) {
            case INDEX_ONE:
                return INDEX_SEVEN;
            case INDEX_TWO:
                return INDEX_SIX;
            case INDEX_THREE:
                return INDEX_FIVE;
            case INDEX_FOUR:
                return INDEX_FOUR;
            case INDEX_FIVE:
                return INDEX_THREE;
            case INDEX_SIX:
                return INDEX_TWO;
            case INDEX_SEVEN:
                return INDEX_ONE;
            case INDEX_EIGHT:
                return 0;
            default:
                return -1;
        }
    }

    /**
     * Returns the index row of the chess board corresponding to the int Entered
     * @param int - the number to convert to its corresponding index row of the chess board
     * @return int - index of the matrix
     */
    public int getEquivalentRow(final int row) {
        switch (row) {
            case 0:
                return INDEX_EIGHT;
            case INDEX_ONE:
                return INDEX_SEVEN;
            case INDEX_TWO:
                return INDEX_SIX;
            case INDEX_THREE:
                return INDEX_FIVE;
            case INDEX_FOUR:
                return INDEX_FOUR;
            case INDEX_FIVE:
                return INDEX_THREE;
            case INDEX_SIX:
                return INDEX_TWO;
            case INDEX_SEVEN:
                return INDEX_ONE;
            default:
                return -1;
        }
    }

    /**
     * Converts the position to a list of two positions: source and target.
     * The references row and column of Source position are in the element 0 of the list
     * and the references row and column of Target position are in the element 1 of the list
     * @return List<int[]> - the List of references to the positions source and target
     */
    public List<int[]> convertPosition() throws Exception {
        positionToMove = new ArrayList<int[]>();
        int[] sourcePosition = new int[SIZE];
        int[] targetPosition = new int[SIZE];
        try {
            sourceRow = getEquivalent(input.substring(INDEX_TWO, INDEX_THREE));
            sourceColumn = getNumber(input.charAt(INDEX_THREE));
            targetRow = getEquivalent(input.substring(INDEX_SEVEN, INDEX_EIGHT));
            targetColumn = getNumber(input.charAt(INDEX_EIGHT));
            sourcePosition[0] = sourceRow;
            sourcePosition[1] = sourceColumn;
            targetPosition[0] = targetRow;
            targetPosition[1] = targetColumn;
        } catch (Exception e) {
            throw new Exception("Value invalid: " + e.getMessage());
        }
        positionToMove.add(0, sourcePosition);
        positionToMove.add(1, targetPosition);
        return positionToMove;
    }
    /**
     * Converts the reference to the position to an array which contains the row and column
     * @return int[] - Array of references to the row and column
     */
    public int[] convertPiecePosition() throws Exception {
        int[] sourcePosition = new int[SIZE];
        try {
            sourceRow = getEquivalent(input.substring(INDEX_TWO, INDEX_THREE));
            sourceColumn = getNumber(input.charAt(INDEX_THREE));
            sourcePosition[0] = sourceRow;
            sourcePosition[1] = sourceColumn;
        } catch (Exception e) {
            throw new Exception("Value invalid: " + e.getMessage());
        }
        return sourcePosition;
    }

    /**
     * Converts position to its correspondent values in the chess board
     * @return String[] - the reference String to the position to display formatted as String
     */
    public String[] convertToDisplay() {
        String[] posToDisplay = new String[2];
        Integer row = (Integer) getEquivalentRow(position.getRow());
        int column = position.getColumn();
        posToDisplay[0] = row.toString();
        posToDisplay[1] = getLetter(column);
        return posToDisplay;
    }
}
