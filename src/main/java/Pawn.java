import java.util.ArrayList;
import java.util.List;

public class Pawn extends Piece implements Promotable {
    private Piece[][] pieces;
    private static final int POS_INI_BLACK = 0;
    private static final int POS_INI_WHITE = 7;

    public Pawn(final Color color, final Position position, final Symbol symbol, final boolean doesExist) {
        super(color, position, symbol, doesExist);
    }

    public Pawn(final Color color, final Position position, final Piece[][] board) {
        super(color, position, Symbol.P, true);
        this.pieces = board;
    }

    /**
     *Change the state and remove the position of the Piece
     * @param piece that will be destroyed
     */
    public void destroy(final Piece piece) {
        piece.setDoesExist(false);
        piece.setPosition(null);
    }

    /**
     * @Override *
     */
    public void die() {
        setPosition(null);
    }

    /**
     * @Override *
     */
    public void move(final Position position) {
        if (isPossibleMove(position)) {
            if (pieces[position.getRow()][position.getColumn()] != null) {
                destroy(pieces[position.getRow()][position.getColumn()]);
            }
            setPosition(new Position(position.getRow(), position.getColumn()));
            setDoesMoved(true);
        }
    }

    /**
     * @Override *
     */
    public boolean isPossibleMove(final Position position) {
        List<Position> positions = getPossibleMoves();
        for (Position poss : positions) {
            if (poss.getColumn() == position.getColumn() && poss.getRow() == position.getRow()) {
                return true;
            }
        }
        return false;
    }

    /**
     * @Override
     * **/
    public List<Position> getPossibleMoves() {
        List<Position> possibles = new ArrayList<>();
        if (getColor() == Color.B) {
            if (!getDoesMoved()) {
                collectPossibleMovesBlackPawnIsNotMoved(getPosition().getRow(), getPosition().getColumn(), possibles);
            } else {
                collectPossibleMovesBlackPawnIsMoved(getPosition().getRow(), getPosition().getColumn(), possibles);
            }
        } else {
            if (!getDoesMoved()) {
                collectPossibleMovesWhitePawnIsNotMoved(getPosition().getRow(), getPosition().getColumn(), possibles);
            } else {
                collectPossibleMovesWhitePawnIsMoved(getPosition().getRow(), getPosition().getColumn(), possibles);
            }
        }
        return possibles;
    }

    /**
     * Collect all available black pawn moves when it hasn't moved yet and send to collection
     * @param row the board's row
     * @param column the board's column
     * @param possibles the List to store all possible moves
     */
    private void collectPossibleMovesWhitePawnIsNotMoved(final int row, final int column, final List<Position> possibles) {
        if (pieces[row - 1][column] == null) {
            possibles.add(new Position(row - 1, column));
            if (pieces[row - 2][column] == null) {
                possibles.add(new Position(row - 2, column));
            } else if (pieces[row - 2][column].getColor() != getColor()) {
                possibles.add(new Position(row - 2, column));
            }
        }
        collectPossibleMovesDiagonalWhitePawn(row, column, possibles);
    }

    /**
     * Collect all available White pawn moves when it has already moved and send to collection
     * @param row the board's row
     * @param column the board's column
     * @param possibles the List to store all possible moves
     */
    private void collectPossibleMovesWhitePawnIsMoved(final int row, final int column, final List<Position> possibles) {
        if (doesRange(row - 1, column)) {
            if (pieces[row - 1][column] == null) {
                possibles.add(new Position(row - 1, column));
            }
            collectPossibleMovesDiagonalWhitePawn(row, column, possibles);
        }
    }

    /**
     * Collect all the available White pawn movements in the diagonals and send the collection
     * @param row the board's row
     * @param column the board's column
     * @param possibles the List to store all possible moves
     */
    private void collectPossibleMovesDiagonalWhitePawn(final int row, final int column, final List<Position> possibles) {
        if (doesRange(row - 1, column - 1)) {
            if (pieces[row - 1][column - 1] != null && pieces[row - 1][column - 1].getColor() != getColor()) {
                possibles.add(new Position(row - 1, column - 1)); // eatLeft
            }
        }
        if (doesRange(row - 1, column + 1)) {
            if (pieces[row - 1][column + 1] != null && pieces[row - 1][column + 1].getColor() != getColor()) {
                possibles.add(new Position(row - 1, column + 1)); // eatRight
            }
        }
    }
    /**
     * Collect all available black pawn moves when it hasn't moved yet and send to collection
     * @param row the board's row
     * @param column the board's column
     * @param possibles the List to store all possible moves
     */
    private void collectPossibleMovesBlackPawnIsNotMoved(final int row, final int column, final List<Position> possibles) {
        if (pieces[row + 1][column] == null) {
            possibles.add(new Position(row + 1, column));
            if (pieces[row + 2][column] == null) {
                possibles.add(new Position(row + 2, column));
            } else if (pieces[row + 2][column].getColor() != getColor()) {
                possibles.add(new Position(row + 2, column));
            }
        }
        collectPossibleMovesDiagonalBlackPawn(row, column, possibles);
    }

    /**
     * Collect all available black pawn moves when it has already moved and send to collection
     * @param row the board's row
     * @param column the board's column
     * @param possibles the List to store all possible moves
     */
    private void collectPossibleMovesBlackPawnIsMoved(final int row, final int column, final List<Position> possibles) {
        if (doesRange(row + 1, column)) {
            if (pieces[row + 1][column] == null) {
                possibles.add(new Position(row + 1, column));
            }
            collectPossibleMovesDiagonalBlackPawn(row, column, possibles);
        }
    }

    /**
     * Collect all the available black pawn movements in the diagonals and send the collection
     * @param row the board's row
     * @param column the board's column
     * @param possibles the List to store all possible moves
     */
    private void collectPossibleMovesDiagonalBlackPawn(final int row, final int column, final List<Position> possibles) {
        if (doesRange(row + 1, column - 1)) {
            if (pieces[row + 1][column - 1] != null && pieces[row + 1][column - 1].getColor() != getColor()) {
                possibles.add(new Position(row + 1, column - 1)); // eatLeft
            }
        }
        if (doesRange(row + 1, column + 1)) {
            if (pieces[row + 1][column + 1] != null && pieces[row + 1][column + 1].getColor() == Color.W) {
                possibles.add(new Position(row + 1, column + 1)); // eatRight
            }
        }
    }

    /**
     * Return true if row and column is in the range [0-7]
     * @param row the board's row
     * @param column the board´s column
     * @return boolean according to the board limits
     */
    private boolean doesRange(final int row, final int column) {
        if (row < 0 || row >= pieces.length) {
            return false;
        }
        if (column < 0 || column >= pieces.length) {
            return false;
        }
        return true;
    }

    /**
     * @Override
     */
    public void promotion(final Position position, final String symbol) {
        //pieces[getPosition().getRow()][getPosition().getColumn()] = null;
        Piece piece = null;
        switch (symbol) {
            case "B":
            piece = new Bishop(getColor(), position, pieces);
                 break;
            case "H":
            piece = new Horse(getColor(), position, pieces);
                break;
            case "Q":
            piece = new Queen(getColor(), position, pieces);
                break;
            case "R":
            piece = new Rook(getColor(), position, pieces);
                break;
            default:
                break;
        }
        pieces[position.getRow()][position.getColumn()] = piece;
        this.die();
    }

    /**
     * Implements method can promote
     * @Override
     */
    public boolean canPromote(final Position position) {
        if (getColor() == Color.B && position.getRow() == POS_INI_WHITE && isPossibleMove(position)) {
            return true;
        }
        if (getColor() == Color.W && position.getRow() == POS_INI_BLACK && isPossibleMove(position)) {
            return true;
        }
        return false;
    }
}
