import java.util.List;
import java.util.ArrayList;

public class ChessBoard {
    public static final int SIZE_BOARD = 8;
    public static final int TYPE_CAST = 4;
    private Piece[][] board;
    private List<Piece> pieces;
    private List<Piece> wPieces;
    private List<Piece> bPieces;
    private boolean turnWhite = true;

    private CreatorPieces cPieces;

    public ChessBoard() {
        wPieces = new ArrayList<Piece>();
        bPieces = new ArrayList<Piece>();
        pieces = new ArrayList<Piece>();
        board = new Piece[SIZE_BOARD][SIZE_BOARD];
        cPieces = new CreatorPieces(wPieces, bPieces, board);
        fillPieces();
    }

    /**
     * Returns a list with the pieces
     * @return List<Piece> - the list of the pieces in the board
     */
    public List<Piece> getPieces() {
        return pieces;
    }

    /**
     * Fill pieces in the board
     */
    public void fillPieces() {
        cPieces.createPieces();
        pieces.addAll(wPieces);
        pieces.addAll(bPieces);
        for (Piece piece: pieces) {
            int x = piece.getPosition().getRow();
            int y = piece.getPosition().getColumn();
            board[x][y] = piece;
        }
        drawPieces();
    }

    /**
     * Fill the board of this object with a piece
     * @param piece - the reference piece to redraw in the board
     */
    public void fillBoard(final Piece piece) {
        int x = piece.getPosition().getRow();
        int y = piece.getPosition().getColumn();
        if (piece.getDoesMoved()) {
            board[x][y] = piece;
            board[piece.getSource().getRow()][piece.getSource().getColumn()] = null;
        }
        drawPieces();
    }

    /**
     * @return board - the chess board
     */
    public Piece[][] getBoard() {
        return board;
    }

    /**
     * Return if it's the white pieces' turn
     * @return
     */
    public boolean getTurn() {
        return turnWhite;
    }
    /**
     * Sets the board of this object
     * @param board - the reference board to set to this object
     */
    public void setBoard(final Piece[][] board) {
        this.board = board;
    }

    /**
     * Sets the white pieces of this object
     * @param wPieces - the reference list of white pieces to set to this object
     */
    public void setWPieces(final List<Piece> wPieces) {
        this.wPieces = wPieces;
    }

    /**
     * Sets the black pieces of this object
     * @param bPieces - the reference list of black pieces to set to this object
     */
    public void setBPieces(final List<Piece> bPieces) {
        this.bPieces = bPieces;
    }

    /**
     * Sets the pieces of this object
     * @param pieces - the reference list of pieces to set to this object
     */
    public void setPieces(final List<Piece> pieces) {
        this.pieces = pieces;
    }

    /**
     * Clear list of pieces and board of this object
     */
    public void clear() {
        pieces.clear();
        board = new Piece[SIZE_BOARD][SIZE_BOARD];
        cPieces.clearPieces();
        turnWhite = true;
    }

    /**
     * Gets a piece of the board of this object
     * @param position - the position of the board
     * @return piece - the reference piece placed in the position of the board
     **/
    public Piece getPiece(final Position position) {
        int x = position.getRow();
        int y = position.getColumn();
        return board[x][y];
    }

    /**
     * Draws board with the pieces in the console
     */
    public void drawPieces() {
        int row = SIZE_BOARD;
        System.out.println("   a   b   c   d   e   f   g   h  ");
        System.out.println("----------------------------------");
        for (int i = 0; i < SIZE_BOARD; i++) {
            for (int j = 0; j < board[i].length; j++) {
                if (board[i][j] == null) {
                    System.out.print(" |  ");
                } else {
                    System.out.print(" |" + (board[i][j].getSymbol().toString() + board[i][j].getColor().toString()));
                }
            }
            System.out.print(" |" + row--);
            System.out.println();
        }
        System.out.println("----------------------------------");
        System.out.println("   a   b   c   d   e   f   g   h  ");
        System.out.println("\n");
    }

    /**
     * @param inputString
     * @return
     */
    public boolean canPromote(final String inputString) throws Exception {
        Parser posToMoveReader = new Parser(inputString);
        List<int[]> positionList;
        positionList = posToMoveReader.convertPosition();
        int sourceRow = positionList.get(0)[0];
        int sourceColumn = positionList.get(0)[1];
        int targetRow = positionList.get(1)[0];
        int targetColumn = positionList.get(1)[1];
        return (board[sourceRow][sourceColumn] instanceof Pawn && ((Pawn) board[sourceRow][sourceColumn]).canPromote(new Position(targetRow, targetColumn)));
    }

    /**
     * @param inputString
     * @param symbol
     */
    public void promoteTo(final String inputString, final String symbol) throws Exception {
        Parser posToMoveReader = new Parser(inputString);
        List<int[]> positionList;
        positionList = posToMoveReader.convertPosition();
        int sourceRow = positionList.get(0)[0];
        int sourceColumn = positionList.get(0)[1];
        int targetRow = positionList.get(1)[0];
        int targetColumn = positionList.get(1)[1];
        ((Pawn) (board[sourceRow][sourceColumn])).promotion(new Position(targetRow, targetColumn), symbol);
        board[sourceRow][sourceColumn] = null;
        drawPieces();
    }

    /**
     * Moves the piece of the board of this object to the reference position
     * @param inputString - the reference string which contains information with the position where to move the reference piece of the board of this object
     */
    public void move(final String inputString) throws Exception {
        Parser posToMoveReader = new Parser(inputString);
        List<int[]> positionList;
        positionList = posToMoveReader.convertPosition();
        int sourceRow = positionList.get(0)[0];
        int sourceColumn = positionList.get(0)[1];
        int targetRow = positionList.get(1)[0];
        int targetColumn = positionList.get(1)[1];
        if (board[sourceRow][sourceColumn].isPossibleMove(new Position(targetRow, targetColumn))) {
                board[sourceRow][sourceColumn].move(new Position(targetRow, targetColumn)); // move should control to destroy
                board[targetRow][targetColumn] = board[sourceRow][sourceColumn];
                board[sourceRow][sourceColumn] = null;
                drawPieces();
                if (turnWhite) {
                    turnWhite = false;
                } else {
                    turnWhite = true;
                }
        } else {
            System.out.println("The movement is not possible");
            drawPieces();
        }
    }

    /**
     * Checks if there is a reference to a piece in the position of the board of this object
     * @param position - the reference position where to check if there is a piece
     * @return true if position has a piece; false otherwise
    */
    public boolean doesExistPiece(final Position position) {
        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board[i].length; j++) {
                if (i == position.getColumn() && j == position.getRow()) {
                    if (board[i][j] == null) {
                        return false;
                    }
                }
            }
        }
        return true;
    }

    /**
     * Displays possible moves of the piece in the board of this object
     * @param piece - a reference String to a piece
     */
    public void displayPossibleMoves(final String inputString) throws Exception {
        Parser inputPosition = new Parser(inputString);
        int row = 0;
        int column = 1;
        int[] positionList;
        String[] posToDisplay;
        positionList = inputPosition.convertPiecePosition();
        int sourceRow = positionList[0];
        int sourceColumn = positionList[1];
        List<Position> possibleMoves = board[sourceRow][sourceColumn].getPossibleMoves();
        if (possibleMoves.isEmpty()) {
            System.out.println("There is not movement");
            drawPieces();
        } else {
            drawPieces();
            for (Position position : possibleMoves) {
                Parser posReader = new Parser(position);
                posToDisplay = posReader.convertToDisplay();
                System.out.println("Possible moves: (" + posToDisplay[row] + posToDisplay[column] + ") ");
            }
        }
    }

    /**
     * @return Piece - the reference Piece type king of this object
     */
    public Piece getKing() {
        Piece king = null;
        for (int i = 0; i < SIZE_BOARD; i++) {
            for (int j = 0; j < SIZE_BOARD; j++) {
                if (board[i][j] instanceof King) {
                    if (!turnWhite && board[i][j].getColor() == Color.B) {
                        king = board[i][j];
                    }
                    if (turnWhite && board[i][j].getColor() == Color.W) {
                        king = board[i][j];
                    }
                }
            }
        }
        return king;
    }

    /**
     * @param movement
     * @return boolean
     */
    public boolean isInCheck() {
        Piece king = getKing();
            if (((King) king).isTheKingInCheck()) {
                return true;
            }
        return false;
    }

    /**
     * Returns who is the winner BLACK or WHITE
     * @return string
     */
    public String getWinner() {
        if (turnWhite) {
            return "BLACK";
        } else {
            return "WHITE";
        }
    }
    /**
     * Returns if the king is in checkmate
     * @return
     */
    public boolean isInCheckmate() {
        Piece king = getKing();
        return ((King) king).checkMate();
    }

    /**
     * Returns if is in your turn
     * @return boolean
     */
    public boolean isValidTurn(final String inputString) throws Exception {
        Parser inputPosition = new Parser(inputString);
        int row = 0;
        int column = 1;
        int[] positionList;
        positionList = inputPosition.convertPiecePosition();
        int sourceRow = positionList[row];
        int sourceColumn = positionList[column];
        return ((board[sourceRow][sourceColumn].getColor() == Color.W && turnWhite) || (board[sourceRow][sourceColumn].getColor() == Color.B && !turnWhite));
    }

    /**
     * Do the castling of the king of this object
     * @param input - the reference object String to do the castling of the king of this object
     */
    public void castKing(final String input) {
        int typeCastling = Integer.parseInt(input);
        King kingInitial = (King) getKing();
        King king;
        Rook rook;
        if (typeCastling == TYPE_CAST) {
            rook = (Rook) kingInitial.shortCastling();
        } else {
            rook = (Rook) kingInitial.largeCastling();
        }
        king = (King) this.getKing();
        this.fillBoard(king, rook);
    }

    /**
     * Fill the board of this object with two pieces
     * @param king - the reference piece King to redraw in the board of this object
     * @param horse - the reference piece Horse to redraw in the board of this object
     */
    private void fillBoard(final King king, final Rook rook) {
        int rowKing = king.getPosition().getRow();
        int columnKing = king.getPosition().getColumn();
        int rowHorse = rook.getPosition().getRow();
        int columnHorse = rook.getPosition().getColumn();
        board[rowKing][columnKing] = king;
        board[rowHorse][columnHorse] = rook;
        turnWhite = false;
        drawPieces();
    }
}
