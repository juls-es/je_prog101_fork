import java.util.List;
import java.util.ArrayList;

public class King extends Piece {

    private Piece[][] board;
    public static final int SIZE_BOARD = 8;
    public static final int INDEX_SEVEN = 7;
    public static final int INDEX_SIX = 6;
    public static final int INDEX_FIVE = 5;
    public static final int INDEX_ZERO = 0;
    public static final int INDEX_THREE = 3;
    public static final int INDEX_TWO = 2;
    public static final int INDEX_ONE = 1;
    public static final int TWO_NEGATIVE = -2;

    public King(final Color color, final Position position, final Piece[][] board) {
        super(color, position, Symbol.K, true);
        this.board = board;
    }

    /**
     * @Override
     */
    public void destroy(final Piece piece) {
        piece.setDoesExist(false);
        piece.setPosition(null);

    }

    /**
     * @Override
     **/
    public void destroy() {
    }

    /**
     * @Override
     **/
    public void die() {
        setDoesExist(false);
        setPosition(null);
    }

    /**
     * @Override
     **/
    public void move(final Position position) {
        if (isPossibleMove(position)) {
            if (board[position.getRow()][position.getColumn()] != null) {
                destroy(board[position.getRow()][position.getColumn()]);
            }
            setPosition(new Position(position.getRow(), position.getColumn()));
            setDoesMoved(true);
        }
    }

    /**
     * @Override
     **/
    public boolean isPossibleMove(final Position position) {
        for (Position pos : getPossibleMoves()) {
            if (pos.getRow() == position.getRow() && pos.getColumn() == position.getColumn()) {
                return true;
            }
        }
        return false;
    }

    /**
     * @Override
     **/
    public List<Position> getPossibleMoves() {
        int row = getPosition().getRow();
        int column = getPosition().getColumn();
        List<Position> possibles = new ArrayList<>();
        for (int i = (row - 1); i < (row + 2); i++) {
            for (int j = column - 1; j <= column + 1; j++) {
                if (i != row || j != column) {
                    if (doesRange(i, j)) {
                        if (board[i][j] == null) {
                            possibles.add(new Position(i, j));
                        } else if (board[i][j].getColor() != getColor()) {
                            possibles.add(new Position(i, j));
                        }
                    }
                }
            }
        }
        return possibles;
    }

    /**
     * Return true if row and column is in the range [0-7]
     *
     * @param row    the board's row
     * @param column the board´s column
     * @return boolean according to the board limits
     */
    private boolean doesRange(final int row, final int column) {
        if (row < 0 || row > board.length - 1) {
            return false;
        }
        if (column < 0 || column > board.length - 1) {
            return false;
        }
        return true;
    }

    /**
     * castling short if the king is not check
     */
    public Piece shortCastling() {
        Piece rook = null;
        if (!isTheKingInCheck()) {
            int row = getPosition().getRow();
            int column = getPosition().getColumn();
            if (!getDoesMoved()) {
                if (board[row][INDEX_SEVEN] instanceof Rook && !board[row][INDEX_SEVEN].getDoesMoved()) {
                    if (board[row][INDEX_FIVE] == null && board[row][INDEX_SIX] == null) {
                        this.setPosition(new Position(row, INDEX_SIX));
                        board[row][INDEX_SIX] = this;
                        board[row][INDEX_SEVEN].setPosition(new Position(row, INDEX_FIVE));
                        board[row][INDEX_FIVE] = board[row][INDEX_SEVEN];
                        board[row][column] = null;
                        board[row][INDEX_SEVEN] = null;
                        board[row][INDEX_FIVE].setDoesMoved(true);
                        rook = board[row][INDEX_FIVE];
                        this.setDoesMoved(true);
                    }
                }
            }
        }
        return rook;
    }

    /**
     * castling large if the king is not check
     */
    public Piece largeCastling() {
        Piece rook = null;
        if (!isTheKingInCheck()) {
            int row = getPosition().getRow();
            int column = getPosition().getColumn();
            if (!getDoesMoved()) {
                if (board[row][INDEX_ZERO] instanceof Rook && !board[row][INDEX_ZERO].getDoesMoved()) {
                    if (board[row][INDEX_THREE] == null && board[row][INDEX_TWO] == null && board[row][INDEX_ONE] == null) {
                        this.setPosition(new Position(row, INDEX_TWO));
                        board[row][INDEX_TWO] = this;
                        board[row][INDEX_ZERO].setPosition(new Position(row, INDEX_THREE));
                        board[row][INDEX_THREE] = board[row][INDEX_ZERO];
                        board[row][INDEX_THREE].setDoesMoved(true);
                        board[row][column] = null;
                        board[row][INDEX_ZERO] = null;
                        rook = board[row][INDEX_THREE];
                        this.setDoesMoved(true);
                    }
                }
            }
        }
        return rook;
    }
    /**
     * Very if the king is in check mate
     * @return true if is checkmate
     */
    public boolean checkMate() {
        if (isTheKingInCheck()) {
            for (Position position : getPossibleMoves()) {
                if (!isTheKingInCheck(position)) {
                    return false;
                }
            }
            return true;
        }
        return false;
    }

    /**
     * Verify if the king´s possiblesMoves is in check
     * @return true if a position is in check
     * @param position
     */
    public boolean isTheKingInCheck(final Position position) {
        int row = position.getRow();
        int column = position.getColumn();
        boolean down = isCheckInDown(row, column);
        boolean up = isCheckInUp(row, column);
        boolean right = isCheckInRight(row, column);
        boolean left = isCheckInLeft(row, column);
        boolean topRight = isCheckInTopRight(row, column);
        boolean topLeft = isCheckInTopLeft(row, column);
        boolean bottomRight = isCheckInBottomRight(row, column);
        boolean bottomLeft = isCheckInBottomLeft(row, column);
        boolean checkHorse = horseCheckingMe(row, column);
        boolean checkMePawn = pawnCheckingMe(row, column);
        if (down || up || right || left || topRight || topLeft || bottomRight || bottomLeft || checkHorse || checkMePawn) {
            return true;
        }
        return false;
    }

    /**
     * Verify if the king is in check
     * @return true if the king is check
     */
    public boolean isTheKingInCheck() {
        int row = getPosition().getRow();
        int column = getPosition().getColumn();
        boolean down = isCheckInDown(row, column);
        boolean up = isCheckInUp(row, column);
        boolean right = isCheckInRight(row, column);
        boolean left = isCheckInLeft(row, column);
        boolean topRight = isCheckInTopRight(row, column);
        boolean topLeft = isCheckInTopLeft(row, column);
        boolean bottomRight = isCheckInBottomRight(row, column);
        boolean bottomLeft = isCheckInBottomLeft(row, column);
        boolean checkHorse = horseCheckingMe(row, column);
        boolean checkMePawn = pawnCheckingMe(row, column);
        if (down || up || right || left || topRight || topLeft || bottomRight || bottomLeft || checkHorse || checkMePawn) {
            return true;
        }
        return false;
    }

    /**
     * verify if the king is check in bottom left
     * @param row king's row
     * @param column king's column
     * @return true if the king is check
     */
    private boolean isCheckInBottomLeft(final int row, final int column) {
        for (int i = 1; i < SIZE_BOARD; i++) {
            if ((row + i) < SIZE_BOARD && (column + i) < SIZE_BOARD) {
                Piece piece = board[row + i][column + i];
                if (piece == null) {
                    continue;
                } else {
                    if (piece instanceof Queen || piece instanceof Bishop) {
                        return  !hasSameColor(piece);
                    }
                    break;
                }
            }
        }
        return false;
    }
    /**
     * verify if the king is check in bottom Right
     * @param row king's row
     * @param column king's column
     * @return true if the king is check
     */
    private boolean isCheckInBottomRight(final int row, final int column) {
        for (int i = 1; i < SIZE_BOARD; i++) {
            if ((row + i) < SIZE_BOARD && (column - i) > -1) {
                Piece piece = board[row + i][column - i];
                if (piece == null) {
                    continue;
                } else {
                    if (piece instanceof Queen || piece instanceof Bishop) {
                        return  !hasSameColor(piece);
                    }
                    break;
                }
            }
        }
        return false;
    }

    /**
     * verify if the king is check in top left
     * @param row king's row
     * @param column king's column
     * @return true if the king is check
     */
    private boolean isCheckInTopLeft(final int row, final int column) {
        for (int i = 1; i < SIZE_BOARD; i++) {
            if ((row - i) > -1 && (column - i) > -1) {
                Piece piece = board[row - i][column - i];
                if (piece == null) {
                    continue;
                } else {
                    if (piece instanceof Queen || piece instanceof Bishop) {
                        return  !hasSameColor(piece);
                    }
                    break;
                }
            }
        }
        return false;
    }
    /**
     * verify if the king is check in top right
     * @param row king's row
     * @param column king's column
     * @return true if the king is check
     */
    private boolean isCheckInTopRight(final int row, final int column) {
        for (int i = 1; i < SIZE_BOARD; i++) {
            if ((row - i) > -1 && (column + i) < SIZE_BOARD) {
                Piece piece = board[row - i][column + i];
                if (piece == null) {
                    continue;
                } else {
                    if (piece instanceof Queen || piece instanceof Bishop) {
                        return  !hasSameColor(piece);
                    }
                    break;
                }
            }
        }
        return false;
    }

    /**
     * verify if the king is check in left
     * @param row king's row
     * @param column king's column
     * @return true if the king is check
     */
    private boolean isCheckInLeft(final int row, final int column) {
        for (int i = column - 1; i > -1; i--) {
            Piece piece = board[row][i];
            if (piece == null) {
                continue;
            } else {
                if (piece instanceof Queen || piece instanceof Rook) {
                    return  !hasSameColor(piece);
                }
                break;
            }
        }
        return false;
    }

    /**
     * verify if the king is check in right
     * @param row king's row
     * @param column king's column
     * @return true if the king is check
     */
    private boolean isCheckInRight(final int row, final int column) {
        for (int i = column + 1; i < SIZE_BOARD; i++) {
            Piece piece = board[row][i];
            if (piece == null) {
                continue;
            } else {
                if (piece instanceof Queen || piece instanceof Rook) {
                    return  !hasSameColor(piece);
                }
                break;
            }
        }
        return false;
    }

    /**
     * verify if the king is check in Up
     * @param row king's row
     * @param column king's column
     * @return true if the king is check
     */
    private boolean isCheckInUp(final int row, final int column) {
        for (int i = row - 1; i > -1; i--) {
            Piece piece = board[i][column];
            if (piece == null) {
                continue;
            } else {
                if (piece instanceof Queen || piece instanceof Rook) {
                    return  !hasSameColor(piece);
                }
                break;
            }
        }
        return false;
    }

    /**
     * verify if the king is check in Down
     * @param row king's row
     * @param column king's column
     * @return true if the king is check
     */
    private boolean isCheckInDown(final int row, final int column) {
        for (int i = row + 1; i < SIZE_BOARD; i++) {
            Piece piece = board[i][column];
            if (piece == null) {
                continue;
            } else {
                if (piece instanceof Queen || piece instanceof Rook) {
                  return  !hasSameColor(piece);
                }
                break;
            }
        }
        return false;
    }

    /**
     * verify if the king is check with Horse
     * @param row king's row
     * @param column king's column
     * @return true if the king is check
     */
    private boolean horseCheckingMe(final int row, final int column) {
        int[] auxRows = {TWO_NEGATIVE, -1, 1, 2};
        int[] auxColumns = {-1, TWO_NEGATIVE, TWO_NEGATIVE, -1};
        for (int j = 0; j < auxRows.length; j++) {
            int newRow = row + auxRows[j];
            int newColumn = auxColumns[j];
            if (doesRange(newRow, column - newColumn)) {
                Piece piece1 = board[newRow][column - newColumn];
                if (verifyInstancesHorse(piece1)) {
                    return true;
                }
            }
            if (doesRange(newRow, column + newColumn)) {
                Piece piece2 = board[newRow][column + newColumn];
                if (verifyInstancesHorse(piece2)) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * verify if the king is check with Pawn
     * @param row king's row
     * @param column king's column
     * @return true if the king is check
     */
    public boolean pawnCheckingMe(final int row, final int column) {
        if (getColor() == Color.W) {
            if (doesRange(row - 1, column - 1)) {
                Piece piece = board[row - 1][column - 1];
                if (verifyInstancePawn(piece)) {
                    return true;
                }
            }
            if (doesRange(row - 1, column + 1)) {
                Piece piece = board[row - 1][column + 1];
                if (verifyInstancePawn(piece)) {
                    return true;
                }
            }
        }

        if (getColor() == Color.B) {
            if (doesRange(row + 1, column - 1)) {
                Piece piece = board[row + 1][column - 1];
                if (verifyInstancePawn(piece)) {
                    return true;
                }
            }
            if (doesRange(row + 1, column + 1)) {
                Piece piece = board[row + 1][column + 1];
                if (verifyInstancePawn(piece)) {
                    return true;
                }
            }
        }
        return  false;
    }

    /**
     * verify if the piece is a pawn and it has a different color than the king
     * @param piece to be evaluated
     * @return true if piece is pawn
     */
    private boolean verifyInstancePawn(final Piece piece) {
        return  piece != null && !hasSameColor(piece)
                && (piece instanceof Pawn);
    }

    /**
     * verify if the piece is a Horse and it has a different color than the king
     * @param piece to be evaluated
     * @return true if piece is Horse
     */
    private boolean verifyInstancesHorse(final Piece piece) {
        return  piece != null && !hasSameColor(piece)
                && (piece instanceof Horse);
    }

    /**
     * verify if the piece has same color than the king
     * @param piece to be evaluated
     * @return true if piece has same color than the king
     */
    private boolean hasSameColor(final Piece piece) {
        return (this.getColor() == piece.getColor());
    }

}
