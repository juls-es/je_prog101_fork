import java.util.ArrayList;
import java.util.List;

public class Horse extends Piece {

    public static final int CANT_HORSE = 4;
    public static  final int POS_INI_ROW_HORSE_B = 0;
    public static  final int POS_INI_ROW_HORSE_W = 7;
    public static final int MAX_INDEX = 7;
    public static final int SIZE_BOARD = 8;
    private Piece[][] board;

    public Horse(final Color color, final Position position, final Piece[][] board) {
        super(color, position, Symbol.H, true);
        this.board = board;
    }

    /**
     * @Override
     */
    public void destroy(final Piece piece) {
        piece.setPosition(null);
        piece.setDoesExist(false);
    }

    /**
     * evaluates if it is possible to move this object to the given position
     * @param position - the reference position of board where this object wants to move
     * @return true if position of board is empty or not ocuppied for other object with the same color
     */
    public boolean isPossibleMove(final Position position) {
        boolean isPossiblePosition;
        for (Position poss: getPossibleMoves()) {
            isPossiblePosition = (poss.getRow() == position.getRow()) && (poss.getColumn() == position.getColumn());
            if (isPossiblePosition) {
                return true;
            }
        }
        return false;
    }

    /**
     * Checks if the reference position is empty or occupied for a piece of different color
     * @param position
     * @return true if there is not a piece or is a piece of different color
     */
    public boolean isPossiblePosition(final Position position) {
        boolean doesEmpty = board[position.getRow()][position.getColumn()] == null;
        return (doesEmpty || board[position.getRow()][position.getColumn()].getColor() != this.getColor());
    }

    /**
     * Change the reference position of this object to the position given
     * @param position - the reference position of board where this object wants to move
     */
    public void move(final Position position) {
        Position source = new Position(getPosition().getRow(), getPosition().getColumn());
        int x = position.getRow();
        int y = position.getColumn();
        List<Position> possiblePositions = getPossibleMoves();
        for (Position poss: possiblePositions) {
            if ((poss.getRow() == x) && (poss.getColumn() == y)) {
                setSource(source);
                setPosition(position);
                setDoesMoved(true);
            }
        }
    }

    /**
     * Gets the possible positions where this object can move
     * @return List<Position> - the list of possibles positions where this object can move
     */
    public List<Position> getPossibleMoves() {
        List<Position> possibles = new ArrayList<Position>();
        final int size = 8;
        final int intValue = -2;
        Position[] list = new Position[size];
        int[] aux = {2, 1, intValue, 1, 2, -1, intValue, -1, 2};
        for (int i = 0; i < size; i++) {
            Position position1 = new Position(getPosition().getRow() - aux[i], getPosition().getColumn() - aux[i + 1]);
                list[i] = position1;
        }
        for (final Position piecePosition : list) {
            if ((piecePosition.getRow() > -1) && (piecePosition.getRow() < SIZE_BOARD)
                && (piecePosition.getColumn() > -1) && (piecePosition.getColumn() < SIZE_BOARD)) {
                if (isPossiblePosition(piecePosition)) {
                    possibles.add(piecePosition);
                }
            }
        }
        return possibles;
    }
}
